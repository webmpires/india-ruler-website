_DRCB.push(function() {
    "use strict";
    var $table = $('.table-train-details'),
        visible = false,
        $btnIntermediateStation = $('.btn-intermediate-station').click(function() {
            if (visible) { $table.find('.station-without-halt').addClass('hide'); } else { $table.find('.station-without-halt').removeClass('hide'); }
            visible = !visible;
        });
    $table.find('.station-with-halt').click(function() {
        this.visible = !this.visible;
        if (this.visible) { $(this).nextUntil('.station-with-halt').not(this).removeClass('hide'); } else { $(this).nextUntil('.station-with-halt').not(this).addClass('hide'); }
    });
    $('.btn-load-map').click(function() { $.lazyLoad('http://maps.googleapis.com/maps/api/js?region=IN&key=AIzaSyD2b3UTkFMLClYno4_mDv7PaPlzNcy7GtY&sensor=false&callback=drawMap'); });
});
var gMap, zoom = 20;
window.drawMap = function() {
    var $map = $('#route-map');
    $map.removeClass('bg-gmap');
    var center = new google.maps.LatLng(22.6098, 77.7694);
    var options = { zoom: zoom, center: center, scrollwheel: false, mapTypeId: google.maps.MapTypeId.ROADMAP }
    gMap = new google.maps.Map($map[0], options);
    var stations = [
        [31.6314, 74.8602, "Amritsar Junction", "ASR", 1],
        [31.5899, 75.0543, "Jandiala", "JNL", 2],
        [31.5199, 75.2913, "Beas", "BEAS", 2],
        [31.4324, 75.4926, "Kartarpur", "KRE", 2],
        [31.3314, 75.5913, "Jalandhar City", "JUC", 10],
        [31.3069, 75.6326, "Jalandhar Cantonment", "JRC", 2],
        [31.2173, 75.7655, "Phagwara Junction", "PGW", 2],
        [31.1294, 75.774, "Goraya", "GRY", 2],
        [31.0166, 75.7859, "Phillaur Junction", "PHR", 2],
        [30.9125, 75.8479, "Ludhiana Junction", "LDH", 15],
        [30.6834, 75.8293, "Ahmadgarh", "AHH", 2],
        [30.5293, 75.8716, "Malerkotla", "MET", 2],
        [30.3731, 75.8663, "Dhuri Junction", "DUI", 4],
        [30.3636, 76.1459, "Nabha", "NBA", 2],
        [30.3443, 76.4044, "Patiala", "PTA", 3],
        [30.4863, 76.5939, "Rajpura Junction", "RPJ", 2],
        [30.3789, 76.7631, "Ambala City", "UBC", 2],
        [30.3387, 76.8267, "Ambala Cantonment", "UMB", 30],
        [30.1661, 76.8619, "Shahbad Markanda", "SHDM", 2],
        [29.9699, 76.8518, "Kurukshetra Junction", "KKDE", 2],
        [29.8316, 76.9256, "Nilokheri", "NLKR", 2],
        [29.7978, 76.9381, "Taraori", "TRR", 2],
        [29.6945, 76.9695, "Karnal", "KUN", 2],
        [29.541, 76.9653, "Gharaunda", "GRA", 2],
        [29.3892, 76.9638, "Panipat Junction", "PNP", 2],
        [29.2403, 77.0033, "Samalkha", "SMK", 2],
        [29.1309, 77.0109, "Ganaur", "GNU", 2],
        [28.9891, 77.017, "Sonipat", "SNP", 2],
        [28.9038, 77.059, "Rathdhana", "RDDE", 2],
        [28.8466, 77.0853, "Narela", "NUR", 2],
        [28.7143, 77.1668, "Adarsh Nagar", "ANDI", 2],
        [28.6683, 77.2003, "Subzi Mandi", "SZM", 2],
        [28.6425, 77.2202, "New Delhi", "NDLS", 30],
        [28.5874, 77.2541, "Hazrat Nizamuddin", "NZM", 2],
        [28.4115, 77.3074, "Faridabad", "FDB", 2],
        [28.3405, 77.3125, "Ballabgarh", "BVH", 2],
        [28.1518, 77.342, "Palwal", "PWL", 2],
        [27.7883, 77.4461, "Kosi Kalan", "KSV", 2],
        [27.48, 77.6733, "Mathura Junction", "MTJ", 5],
        [27.194, 77.9968, "Raja Ki Mandi", "RKM", 2],
        [27.1578, 77.9899, "Agra Cantonment", "AGC", 8],
        [26.6975, 77.9059, "Dhaulpur", "DHO", 2],
        [26.5008, 78.0037, "Morena", "MRA", 2],
        [26.216, 78.1819, "Gwalior", "GWL", 5],
        [25.8812, 78.331, "Dabra", "DBA", 2],
        [25.7305, 78.4089, "Sonagir", "SOR", 2],
        [25.6408, 78.4561, "Datia", "DAA", 2],
        [25.4434, 78.5526, "Jhansi Junction", "JHS", 10],
        [25.2383, 78.4522, "Babina", "BAB", 2],
        [25.1426, 78.394, "Basai", "BZY", 2],
        [25.0396, 78.4146, "Talbahat", "TBT", 2],
        [24.688, 78.3953, "Lalitpur", "LAR", 2],
        [24.5477, 78.3423, "Jakhalaun", "JLN", 2],
        [24.4495, 78.3221, "Dhaura", "DUA", 2],
        [24.1711, 78.1833, "Bina Junction", "BINA", 5],
        [24.0538, 78.0822, "Mandi Bamora", "MABA", 2],
        [23.8453, 77.9447, "Ganj Basoda", "BAQ", 2],
        [23.6841, 77.9121, "Gulabhganj", "GLG", 2],
        [23.5226, 77.8152, "Vidisha", "BHS", 2],
        [23.488, 77.7357, "Sanchi", "SCI", 2],
        [23.2672, 77.4134, "Bhopal Junction", "BPL", 5],
        [23.2223, 77.4397, "Bhopal Habibganj", "HBJ", 2],
        [23.0937, 77.5111, "Mandi Dip", "MDDP", 2],
        [22.9949, 77.5875, "Obaidulla Ganj", "ODG", 2],
        [22.7871, 77.6814, "Budni", "BNI", 2],
        [22.7533, 77.7161, "Hoshangabad", "HBD", 2],
        [22.6082, 77.767, "Itarsi Junction", "ET", 5],
        [22.4704, 77.4808, "Banapura", "BPF", 2],
        [22.3761, 77.23, "Timarni", "TBN", 2],
        [22.3381, 77.1005, "Harda", "HD", 2],
        [22.166, 76.859, "Khirkiya", "KKN", 2],
        [21.9635, 76.7076, "Chhanera", "CAER", 2],
        [21.9276, 76.4446, "Talvadya", "TLV", 2],
        [21.8239, 76.3532, "Khandwa", "KNW", 5],
        [21.4639, 76.3971, "Nepanagar", "NPNR", 2],
        [21.3346, 76.1988, "Burhanpur", "BAU", 2],
        [21.2222, 76.0446, "Raver", "RV", 2],
        [21.1612, 75.9756, "Nimbhora", "NB", 2],
        [21.1111, 75.9106, "Savda", "SAV", 2],
        [21.047, 75.7884, "Bhusaval", "BSL", 10],
        [21.0182, 75.563, "Jalgaon Junction", "JL", 2],
        [20.6686, 75.3485, "Pachora Junction", "PC", 2],
        [20.4642, 74.9991, "Chalisgaon Junction", "CSN", 5],
        [20.2491, 74.4361, "Manmad Junction", "MMR", 5],
        [19.948, 73.8423, "Nasik Road", "NK", 5],
        [19.8971, 73.8369, "Devlali", "DVL", 2],
        [19.6944, 73.5627, "Igatpuri", "IGP", 5],
        [19.2352, 73.1303, "Kalyan", "KYN", 3],
        [19.1859, 72.9755, "Thane", "TNA", 3],
        [19.0172, 72.843, "Dadar (Central)", "DR", 3],
        [18.9441, 72.8365, "Mumbai", "CSTM", 1]
    ];
    var positions = [],
        markers = [],
        destIndex = stations.length - 1;
    var min = [180, 180],
        max = [-180, -180];
    var len = stations.length;
    for (var i = 0; i < len; i++) {
        var station = stations[i];
        if (stations[i][0] == null || stations[i][1] == null) {
            continue;
        }
        positions.push(new google.maps.LatLng(station[0], station[1]));
        min[0] = Math.min(min[0], station[0]);
        min[1] = Math.min(min[1], station[1]);
        max[0] = Math.max(max[0], station[0]);
        max[1] = Math.max(max[1], station[1]);
        if (station[4]) {
            var icon = '/travel/images/' + ((i == destIndex) ? 'railway2' : (i > 0 ? 'railway3' : 'railway1')) + '.png';
        } else {
            var icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==';
        }
        markers.push(new google.maps.Marker({ position: positions[i], map: gMap, title: station[2] + ' (' + station[3] + ')', icon: icon }));
    }
    gMap.setCenter(new google.maps.LatLng(((max[0] + min[0]) / 2.0), ((max[1] + min[1]) / 2.0)));
    gMap.fitBounds(new google.maps.LatLngBounds(new google.maps.LatLng(min[0], min[1]), new google.maps.LatLng(max[0], max[1])));
    var trainRoute = new google.maps.Polyline({ map: gMap, path: positions, strokeColor: '#0070B1', strokeOpacity: 0.6, strokeWeight: 5 });
    $('.route-map-wrapper .btn-group > .btn').click(function() {
        var $t = $(this);
        if ($t.is('.active')) return;
        $t.button('toggle');
        var center = gMap.getCenter(),
            bounds = gMap.getBounds(),
            zoom = gMap.getZoom();
        if ($t.text() == 'Normal') {
            zoom--;
            $map.height(250);
        } else {
            zoom++;
            $map.height(600);
        }
        google.maps.event.trigger(gMap, "resize");
        gMap.setCenter(center);
        gMap.fitBounds(bounds);
        gMap.setZoom(zoom);
    }).removeClass('disabled');
}
