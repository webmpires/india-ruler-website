var dropDownData;

var app = angular.module("indiaruler", []);

app.controller('formCtrl', ['$scope', '$http', '$timeout', function($scope, $http, $timeout){
	$scope.data = {};
	$scope.loadedData = false;
	$scope.inProgress = false;

	$scope.onTrainNumberChange = function () {
		$scope.loadedData = false;		
	};

	$scope.loadData = function () {
		if(!$scope.data.train_number) {
			return false;
		}
		$scope.inProgress = true;
		var value = $("#trainnumber").val().replace(/[^0-9.]/g, '');
		$http.post("/api/stations", {"train_number": value}).then(function (response) {
			if(response.data instanceof Array && response.data.length > 0) {
				dropDownData = response.data;
				$scope.scheduleData = response.data;
				$scope.destScheduleData = angular.copy(response.data);
				$scope.data.source = $scope.scheduleData[0].code;
				$scope.data.destination = $scope.destScheduleData[$scope.destScheduleData.length - 1].code;
				$scope.loadedData = true;
				$scope.inProgress = false;
			} else {
				$scope.inProgress = false;
				alert("Could not get stations information.");
			}
		});
	};
}]);

app.directive('datePicker', ['$timeout', function ($timeout) {
	return {
		restrict: "A",
		link: function (scope, iElement, iAttrs) {
			$timeout(function () {
				$(iElement[0]).datepicker({
					dateFormat: "yy-mm-dd"
				});
			}, 1000);
		}
	}
}]);

$(document).ready(function() {
	$("#train-form").on('submit', function(e){
		e.preventDefault();
		var trainnumber, src, dest, date, age, quota, tmpSrc, srcCode, tmpDest, destCode, queryParams, url;
		
		if(!dropDownData) {
			return;
		}
		
		trainnumber = $("#trainnumber").val();        
		src = $("#source").val();
		dest = $("#dest").val();
		date = $("#date").val();
		age = $("#age").val();
		quota = $("#quota").val();
		
		if(trainnumber.match(/\d+/g)[0].length === trainnumber.length){
			trainnumber = availableTags.filter(function(trainName){
				return trainName.indexOf(trainnumber) !== -1;
			})[0];
		}

		queryParams = {};
		if(src !== dropDownData[0].code) {
			queryParams.source = src;
		}

		if(dest !== dropDownData[dropDownData.length - 1].code) {
			queryParams.destination = dest;
		}

		if(date !== moment().format("YYYY-MM-DD")) {
			queryParams.date = date;
		}

		if(age !== "30") {
			queryParams.age = age;
		}

		if(quota !== 'GN') {
			queryParams.quota = quota;
		}

		var url = trainnumber.replace(/\D+/g, '');
		
		if(Object.keys(queryParams).length) {
			url += "?" + $.param(queryParams);
		}

		window.location.href = window.location.origin + '/fare-enquiry/' + url;

		// $(this).submit();
	});
});