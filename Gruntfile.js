module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1,
				keepSpecialComments: 0
			},
			target: {
				files: {
					'assets/dist/main.css': [
						'assets/build/css/bootstrap_themed.css',
						'assets/build/js/jquery-ui/jquery-ui.min.css',
						'assets/build/css/ie10-viewport-bug-workaround.css',
						'assets/build/css/starter-template.css',
						'assets/build/css/min.css'
					]
				}
			}
		},
		concat: {
			options: {
				stripBanners: true,
				banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
			    '<%= grunt.template.today("yyyy-mm-dd") %> */',
			},
			dist: {
				src: [
					'assets/vendors/jquery/dist/jquery.min.js',
					'assets/vendors/bootstrap/dist/js/bootstrap.min.js',
					'assets/build/js/jquery-ui/jquery-ui.custom.min.js',
					'assets/build/js/autocomplete.js',
					'assets/build/js/moment.min.js',
					'assets/build/js/ie10-viewport-bug-workaround.js'
				],
				dest: 'assets/dist/main.js',
			},
		},
		watch: {
		  css: {
		    files: [
					'assets/build/css/min.css', 
					'assets/build/js/autocomplete.js',
					'assets/dist/app.js'
				],
		    tasks: ['cssmin', 'concat'],
		    options: {
		      spawn: false,
		    },
		  },
		},
	});

	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['cssmin', 'concat', 'watch']);

};