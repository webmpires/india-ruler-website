-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: aa1c1u4pejxoitt.ct0xwljld0sr.ap-south-1.rds.amazonaws.com:3306
-- Generation Time: Apr 26, 2017 at 02:01 AM
-- Server version: 5.6.34-log
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pnrstatus`
--

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `type` int(2) NOT NULL COMMENT '0 = notification, 1 = email',
  `user_id` int(11) DEFAULT NULL,
  `pnr_id` int(11) DEFAULT NULL,
  `status` tinyint(2) DEFAULT '0' COMMENT '0 = Pending, 1 = Sent, 2 = Failed'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `installation_id` varchar(100) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `installation_id`, `email`, `created_at`) VALUES
(24, 'f7e7c2c3cf553925274b94e5123fd0f835a18853b581fa5fb035a15545272df', NULL, '2017-04-24 17:45:19'),
(25, '9a67a36bbd69952bc3dfc3edcc47c94e1507f2f2e1823225328a3c080ef73', NULL, '2017-04-25 06:14:42'),
(26, 'cfb2339e9638f6dfa26318215a09d61489fcfdb77d7855424f6d69062e824', NULL, '2017-04-25 17:06:39'),
(27, 'd818dc8b2bd75bf7677139852afde02aed38671c28356f7736f2088e66b37a9', NULL, '2017-04-25 18:43:54'),
(28, 'fd8467de1234a254931467bf43eceda1f861dfc4374470ec6f6abec28c48d7', NULL, '2017-04-25 20:28:46'),
(29, '4b27cb9c119ea574111629a5508e53c722d86da8236e2ada2e11faafc34c925', 'ferlee007@gmail.com', '2017-04-26 01:50:38');

-- --------------------------------------------------------

--
-- Table structure for table `user_pnr_queries`
--

CREATE TABLE `user_pnr_queries` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pnr_info` text NOT NULL,
  `doj` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_pnr_queries`
--

INSERT INTO `user_pnr_queries` (`id`, `user_id`, `pnr_info`, `doj`, `created_at`, `updated_at`) VALUES
(11, 29, '{"response_code":200,"to_station":{"name":"PARAMAKKUDI","code":"PMK"},"pnr":"4130419252","train_name":"","train_start_date":{},"train_num":"22661","boarding_point":{"name":"TAMBARAM","code":"TBM"},"chart_prepared":"N","from_station":{"name":"TAMBARAM","code":"TBM"},"reservation_upto":{"name":"PARAMAKKUDI","code":"PMK"},"total_passengers":2,"doj":"27-4-2017","error":false,"passengers":[{"booking_status":"RAC 46,GNWL","current_status":"Confirmed","no":1,"coach_position":0},{"booking_status":"RAC 47,GNWL","current_status":"Confirmed","no":2,"coach_position":0}],"class":"SL","failure_rate":1.6903457719958919}', '2017-04-27', '2017-04-26 01:51:05', '2017-04-26 01:51:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_pnr_queries`
--
ALTER TABLE `user_pnr_queries`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `user_pnr_queries`
--
ALTER TABLE `user_pnr_queries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
