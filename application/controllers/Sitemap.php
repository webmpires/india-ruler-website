<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model('seo_model');
		// Your own constructor code
    }

	public function index() {
		header("Content-type: text/xml");
		echo file_get_contents("./assets/sitemap.xml");
	}

	public function admin() {
		$this->load->view("sitemap");
	}

	public function populate($for = '') {
		switch ($for) {
			case 'website':
				$this->seo_model->map_websitelinks();
				break;

			case 'running_status':
				$this->seo_model->map_trainRunningStatus();
				break;

			case 'schedule':
				$this->seo_model->map_trainSchedule();
				break;

			case 'seat_avail':
				$this->seo_model->map_seatAvail();
				break;
			
			case 'fare_enquiry':
				$this->seo_model->map_fairEnquiry();
				break;

			 case 'trains_bw_stations':
                    $this->seo_model->map_trainsBetweenStations();
                    break;

			case 'live_station':
				$this->seo_model->map_liveStation();
				break;

			default:
				echo "ok";
				break;
		}
	}

	public function stations() {
		$data = file_get_contents("./assets/stations.json");
        $decoded_data = json_decode($data);
        $dataArray = array();
        // foreach ($decoded_data as $val) {
        //     $urlSlug = implode("-", explode(' ', $val));
        //     $dataArray[] = array(
        //         'loc' => base_url() . $urlSlug,
        //         'timestamp' => date('Y-m-d H:i:s'),
        //         'priority' => 0.8
        //     );
        // }
        foreach ($decoded_data as $val) {
            $tmp = explode(' - ', $val);
           	$dataArray[] = array(
                'station_desc' => $val,
                'station_name' => $tmp[0],
                'station_code' => $tmp[1]
            );
        }

        $this->db->insert_batch("stations", $dataArray);
	}

	public function trains() {
		$data = file_get_contents("./assets/train_status.json");
        $decoded_data = json_decode($data);
        $dataArray = array();
        // foreach ($decoded_data as $val) {
        //     $urlSlug = implode("-", explode(' ', $val));
        //     $dataArray[] = array(
        //         'loc' => base_url() . $urlSlug,
        //         'timestamp' => date('Y-m-d H:i:s'),
        //         'priority' => 0.8
        //     );
        // }
        foreach ($decoded_data as $val) {
            $tmp = explode(' ', $val);
           	$train_number = $tmp[0];
           	unset($tmp[0]);
           	$train_name = implode(' ', $tmp);
           	$dataArray[] = array(
                'train_desc' => $val,
                'train_number' => $train_number,
                'train_name' => $train_name
            );
        }

        $this->db->insert_batch("trains", $dataArray);
	}
}
