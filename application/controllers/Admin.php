<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('admin/login');
	}

	public function stations() {
		$data = file_get_contents("./hacked/stations.json");
        $decoded_data = json_decode($data);
		$dataArray = array();
        foreach ($decoded_data as $val) {
            $dataArray[] = array(
                'station_location' => $val->location,
                'station_name' => $val->station_name,
                'station_code' => $val->station_code
            );
        }
        // pr($dataArray);
        $this->db->insert_batch('stations', $dataArray);
	}
}
