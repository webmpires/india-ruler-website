<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model('home_model');
		$this->load->model('seo_model');
		$this->load->model('api_model');
		// Your own constructor code
    }
	
	public function index()
	{
		$this->load->view('home/index');
	}

	public function live_train_status($train_number = null)
	{

		// format date
		$dateSlug = isset($_GET['date']) ? $_GET['date'] : '';
		
		if (isset($train_number) && !empty($train_number)) {
			if(empty($dateSlug)){
				$date = date('Ymd');
			} else {
				switch ($dateSlug) {
					case 'tomorrow':
						$date = date('Ymd', strtotime('+1 day'));
						break;
					case 'yesterday':
						$date = date('Ymd', strtotime('-1 day'));
						break;
					case '2daysago':
						$date = date('Ymd', strtotime('-2 day'));
						break;
					case '3daysago':
						$date = date('Ymd', strtotime('-3 day'));
						break;
					case '4daysago':
						$date = date('Ymd', strtotime('-4 day'));
						break;
					default:
						break;
				}
			}
			
			$data = $this->home_model->getLiveTrainStatus($train_number, $date);

			/** Determine what next station is */
			$nextStation = ''; $nextStationIndex = 0;
			for ($i = 0; $i < count($data['route']); $i++) { 
				if(empty($data['route'][$i]['has_arrived'])) {
					$nextStation = $data['route'][$i];
					$nextStationIndex = $i;
					break;
				}
			}

			/** Check if data has inconsistency for next station
			 * 	After next station if any of the station has has_arrived = 1 then thats an onconsistency  
			 */
			$inconsistent = false;
			for ($i = ($nextStationIndex + 1); $i < count($data['route']); $i++) { 
				if($data['route'][$i]['has_arrived'] == 1) {
					$inconsistent = true;
					break;
				}
			}

			$scheduleData = $this->home_model->trainSchedule($train_number);
			if(!empty($scheduleData['train'])) {
				$train_name = $scheduleData['train']['name'];

				if(isset($data['route']) && count($data['route']) > 0) {
					// insert link in sitemap if valid
					$this->seo_model->insertAndRepopulate('running_status', base_url(uri_string()));
				}

				$viewData = array(
					"data" => $data,
					"train_name" => $train_name,
					"train_number" => $train_number,
					"schedule_data" => $scheduleData,
					"date" => $date,
					"dateSlug" => $dateSlug,
					"nextStation" => $nextStation,
					"inconsistent" => $inconsistent
				);
			} else {
				redirect(base_url() . 'train-running-status?error=true', 'location');
			}

			$this->load->view('home/live_train_status', $viewData);
		} else {
			$this->load->view('home/live_train_status');	
		}
		
	}

	public function pnr_status()
	{
		if($_POST) {
			$data = $this->home_model->getPnrStatus();
			$this->load->view('home/pnr_status', $data);
		} else {
			$this->load->view('home/pnr_status');
		}
	}

	public function cancelled_trains()
	{		
		$data = $this->home_model->getCancelledTrains();
		
		$view_data = array("data" => $data);
		
		if($this->input->post('date')) {
			$view_data['date'] = $this->input->post('date');
		}

		$this->load->view('home/cancelled_trains', $view_data);
	}

	public function seat_availability_by_train_number($train_number = '')
	{
		if (!empty($train_number)) {
			$scheduleData = $this->home_model->trainSchedule($train_number);

			if(isset($scheduleData['route'])) {
				$routeData = $scheduleData['route'];
				$sourceCode = $routeData[0]['code'];
				$destinaionCode = $routeData[count($routeData) - 1]['code'];
			
				$quota = 'GN';

				$data = array();

				for ($i = 0; $i < 10; $i++) { 
					if($i == 0) {
						$dt = date('Y-m-d');
					} else {
						$d = '+' . $i . ' day';
						$dt = date('Y-m-d', strtotime($d));
					}

					$dataFromRequest = $this->home_model->getSeatAvailibility($sourceCode, $destinaionCode, $dt, $quota);
					$trainData = array("train" => array());

					for($j = 0; $j < count($dataFromRequest['train']); $j++) {
						if($dataFromRequest['train'][$j]['number'] == $train_number) {
							$dataFromRequest['train'][$j]['date'] = $dt;
							array_push($trainData['train'], $dataFromRequest['train'][$j]);
						}
					}
					array_push($data, $trainData);
				}


				$info = !empty($data[0]['train']) ? $data[0]['train'][0] : (!empty($data[1]['train']) ? $data[1]['train'][0] : array());
				
				if(empty($info)) {
					redirect(base_url() . 'seat-availability?error=true', 'location');
				}
				if(isset($info) && count($data[0]["train"]) > 0) {
					// insert link in sitemap if valid
					$this->seo_model->insertAndRepopulate('seat_avail', base_url(uri_string()));
				}


				$dataArray = array(
					"source" => $sourceCode,
					"destination" => $destinaionCode,
					"quota" => $quota,
					"searchByTrain" => true,
					"info" => $info
				);
				
				$viewData = array("data" => $data, "formData" => $dataArray);

				$this->load->view('home/seat_availability_train', $viewData);
			} else {
				redirect(base_url() . 'seat-availability?error=true', 'location');
			}
		} else {
			$this->load->view('home/seat_availability');
		}
	}

	public function seat_availability($sourceCode = '', $destinaionCode = '', $sourceName = '', $destName = '')
	{
		if (!empty($sourceCode) && !empty($destinaionCode)) {
			$date = isset($_GET['date']) ? $_GET['date'] : date('Y-m-d');
			$quota = isset($_GET['quota']) ? $_GET['quota'] : 'GN';

			$data = $this->home_model->getSeatAvailibility($sourceCode, $destinaionCode, $date, $quota);
			if(isset($data["train"]) && count($data["train"]) > 0) {
				// insert link in sitemap if valid
				$this->seo_model->insertAndRepopulate('seat_avail', base_url(uri_string()));
			}
			$data['sourceName'] = strtoupper(implode(' ', explode('-', $sourceName)));
			$data['destName'] = strtoupper(implode(' ', explode('-', $destName)));
			$dataArray = array(
				"source" => $sourceCode,
				"destination" => $destinaionCode,
				"date" => $date,
				"quota" => $quota,
				"searchByTrain" => false
			);

			$viewData = array("data" => $data, "formData" => $dataArray);

			$this->load->view('home/seat_availability', $viewData);
		} else {
			$this->load->view('home/seat_availability');
		}
	}

	public function seat_avail_check() {
		$data = $this->home_model->checkSeatAvailibility($_GET);
		// pr($data);
		$this->load->view('home/seat_availability_check', $data);
	}

	public function fare_enquiry($train_number = '') {
		if(!empty($train_number)) {

			$params = array("train_number" => $train_number);
			
			if(isset($_GET['source']) === false || isset($_GET['destination']) === false) {
				
				$scheduleData = $this->home_model->trainSchedule($train_number);
				$routeData = $scheduleData['route'];
				$params["sourceCode"] =  isset($_GET['source']) ? $_GET['source'] : $routeData[0]['code'];
				$params["destCode"] =  isset($_GET['destination']) ? $_GET['destination'] : $routeData[count($routeData) - 1]['code'];
			} else {
				$params["sourceCode"] =  $_GET['source'];
				$params["destCode"] =  $_GET['destination'];
			}
			
			$params['date'] = isset($_GET['date']) ? $_GET['date'] : date('Y-m-d');
			$params['age'] = isset($_GET['age']) ? $_GET['age'] : '30';
			$params['quota'] = isset($_GET['quota']) ? $_GET['quota'] : 'GN';

			$data = $this->home_model->fareInquiry($params);

			if(isset($data["quota"])) {
				// insert link in sitemap if valid
				$this->seo_model->insertAndRepopulate('fare_enquiry', base_url(uri_string()));
			}

			$this->load->view('home/fare_enquiry', $data);
		} else {
			$this->load->view('home/fare_enquiry');
		}
	}

	public function train_station($sourceCode = '', $destinaionCode = '', $sourceName = '', $destName = '')
	{
		if (!empty($sourceCode) && !empty($destinaionCode)) {
			$data = $this->home_model->trainBetweenStation($sourceCode, $destinaionCode);
			if(isset($data['train']) && count($data['train']) > 0) {
				// insert link in sitemap if valid
				$this->seo_model->insertAndRepopulate('trains_bw_stations', base_url(uri_string()));
			}
			$data['sourceName'] = strtoupper(implode(' ', explode('-', $sourceName)));
			$data['destName'] = strtoupper(implode(' ', explode('-', $destName)));
			$this->load->view('home/trains-between-stations', $data);			
		} else {
			$this->load->view('home/trains-between-stations');
		}
	}

	public function train_schedule($train_number = '')
	{
		if ($train_number) {

			// fetch data
			$data = $this->home_model->trainSchedule($train_number);
				
			// if there is a data then insert it into sitemap
			if(isset($data['route']) && count($data['route']) > 0) {
				$this->seo_model->insertAndRepopulate('schedule', base_url(uri_string()));
			}

			$this->load->view('home/train_schedule', $data);
		} else {
			$this->load->view('home/train_schedule');
		}
	}

	public function live_station($station_code = '')
	{
		if (!empty($station_code)) {
			
			$time = isset($_GET['time']) ? isset($_GET['time']) : '24';
			$data = $this->home_model->liveStation($time, strtoupper($station_code));
			if(isset($data['total']) && isset($data['train']) && count($data['train']) > 0) {
				// insert link in sitemap if valid
				$this->seo_model->insertAndRepopulate('live_station', base_url(uri_string()));
			}
			$tmp = explode('-', $this->uri->segment(2));
			$data['name'] = strtoupper(implode(' ', array_slice($tmp, 1)));
			$this->load->view('home/live_station', $data);	
		} else {
			$this->load->view('home/live_station');
		}
	}

	public function getStations() {
		$input = file_get_contents('php://input');
		$input = json_decode($input);
		if(isset($input->train_number)) {
			$scheduleData = $this->home_model->trainSchedule($input->train_number);
			if(isset($scheduleData["route"])) {
				$data = array();
				foreach ($scheduleData["route"] as $value) {
					$data[] = array("station_name" => $value["fullname"], "code" => $value["code"]);
				}
				echo json_encode($data);
			} else {
				echo json_encode(array());
			}
		} else {
			echo "";
			exit();
		}
	}
}
