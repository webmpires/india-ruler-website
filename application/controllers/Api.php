<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->model('api_model');
    }

	public function index() {
		echo "404";
	}

	public function matching_stations() {
		if(isset($_GET['q'])) {
			header("Content-type: application/json");
			echo json_encode($this->api_model->returnMatchingStations($_GET['q']));
		} else {
			header("Content-type: application/json");
			echo "[]";
		}
	}

	public function matching_trains() {
		if(isset($_GET['q'])) {
			header("Content-type: application/json");
			echo json_encode($this->api_model->returnMatchingTrains($_GET['q']));
		} else {
			header("Content-type: application/json");
			echo "[]";
		}
	}
}
