<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Seat Availbility</title>
    <?php $this->load->view("include/header") ?>
</head>

<body>

<?php $this->load->view("include/nav") ?>
<div class="container custom-container">
    <h1>Seat Availbility</h1>
    <?php if (count($data) > 0) { ?>
        <ul class="breadcrumb">
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/"
                                                                               itemprop="url"><span itemprop="title">Indiaruler</span></a>
            </li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a
                    href="https://indiaruler.com/seat-availability" itemprop="url"><span itemprop="title">Seat Availability</span></a>
            </li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb" class="active">
                <a
                    href="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"
                    itemprop="url">
                    <span itemprop="title">
                        <?php
                            echo $formData['info']['number'] . " " . $formData['info']['name'];
                        ?>
                    </span>
                </a>
            </li>
        </ul>
        <?php 

            $classes = '';
            $i = 0;
            foreach ($formData['info']['classes'] as $v) {
                if ($v['available'] == "Y") {
                    if ($i !== 0) {
                        $classes .= " / ";
                    }
                    $classes .= $v['class-code'] . " ";
                    $i++;
                }
            }

            ?>
            <h1><?php echo $formData['info']['number'] . " " . $formData['info']['name']; ?> Seat Availbility</h1>
            <p><?php echo $formData['info']['number']; ?> seat availability is shown
                below. <?php echo $formData['info']['name']; ?>(<?php echo $formData['info']['number']; ?>)
                travelling from <?php echo $formData['info']['from']['name']; ?>
                to <?php echo $formData['info']['to']['name']; ?>, has seat availability in <?php echo $classes; ?>.
                Further details of seat
                availability available in respective class links..
            </p>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-condensed table-striped" id="listTable">
                    <thead>
                    <th class="text-center">Date</th>
                    <th class="text-center">Train Number</th>
                    <th class="text-center">Classes(Click to check availability)</th>
                    <!-- <th>
                        <table class="table table-bordered table-condensed">
                            <tr>
                                <td colspan="7" class="text-center">Day Run</td>
                            </tr>
                            <tr>
                                <td>M</td>
                                <td>T</td>
                                <td>W</td>
                                <td>T</td>
                                <td>F</td>
                                <td>S</td>
                                <td>S</td>
                            </tr>
                        </table>
                    </th> -->
                    </thead>
                    <tbody>
                    <?php foreach ($data as $value) { 
                            if(count($value['train'])) {
                        ?>
                        <tr>
                            <td class="text-center">
                                <?php echo $value['train'][0]['date'];?>
                            </td>
                            <td class="text-center"><?php echo $formData['info']['number'];?></td>
                            <td class="text-center"><?php $i = 0;
                                foreach ($value['train'][0]['classes'] as $v) {
                                    if ($v['available'] == "Y") { ?>
                                        <?php
                                        if ($i !== 0) {
                                            echo " / ";
                                        }
                                        ?>
                                        <a href="<?php echo base_url(); ?>main/seat_avail_check?<?php
                                        $tmp = $formData;
                                        unset($tmp['info']);
                                        $queryData = $tmp;
                                        $queryData["class"] = $v['class-code'];
                                        $queryData["train_number"] = $value['train'][0]['number'];
                                        unset($queryData["searchByTrain"]);
                                        echo http_build_query($queryData); ?>
                                             ">
                                            <?php echo $v['class-code'] . " "; ?></a>
                                        <?php $i++;
                                    }
                                } ?></td>
                            <!-- <td>
                                <table class="table table-bordered">
                                    <tr><?php foreach ($value['days'] as $va) {
                                            echo "<td>" . $va['runs'] . "</td>";
                                        } ?></tr>
                                </table>
                            </td> -->
                        </tr>
                    <?php } else { ?>
                        <tr>
                            <td class="text-center">--</td>
                            <td class="text-center">--</td>
                            <td class="text-center">--</td>
                        </tr>
                    <?php } 
                        } 
                    ?>
                    </tbody>
                </table>
                <br>
                <?php if (isset($formData) && $formData['searchByTrain'] == true) { ?>
                    <p>
                        <?php
                        $train_number = $formData['info']['number'];
                        $train_name = $formData['info']['name'];

                        $linkInfo = getLink('running_status', array("train_number" => $train_number, "train_name" => $train_name)); ?>
                        <a class="btn btn-info btn-interlink" href="<?php echo $linkInfo['href']; ?>" target="_blank"
                           title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>

                        <?php $linkInfo = getLink('fare_inquiry', array("train_number" => $train_number, "train_name" => $train_name)); ?>
                        <a class="btn btn-info btn-interlink" href="<?php echo $linkInfo['href']; ?>" target="_blank"
                           title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>

                        <?php $linkInfo = getLink('schedule', array("train_number" => $train_number, "train_name" => $train_name)); ?>
                        <a class="btn btn-info btn-interlink" href="<?php echo $linkInfo['href']; ?>" target="_blank"
                           title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>
                    </p>
                <?php } ?>
                <div class="row">
                    <div class="col-md-6">
                        <h4>Classes of Accommodation</h4>
                        <table class="table table-bordered">
                            <tr>
                                <td>1A</td>
                                <td>First class Air-conditioned (also Executive Class)</td>
                            </tr>
                            <tr>
                                <td>2A</td>
                                <td>Air-conditioned 2-tier sleeper</td>
                            </tr>
                            <tr>
                                <td>FC</td>
                                <td>First Class</td>
                            </tr>
                            <tr>
                                <td>3A</td>
                                <td>Air-conditioned 3-tier sleeper</td>
                            </tr>
                            <tr>
                                <td>CC</td>
                                <td>Air-conditioned chair car</td>
                            </tr>
                            <tr>
                                <td>SL</td>
                                <td>Sleeper class</td>
                            </tr>
                            <tr>
                                <td>2S</td>
                                <td>Second-class Sitting</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } elseif (
    (count($data) == 0)
    ) { ?>
        <div class="alert alert-dismissible alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            Could not find any data.
        </div>
    <?php } ?>
</div>
<?php $this->load->view('include/footer'); ?>
</body>

</html>
