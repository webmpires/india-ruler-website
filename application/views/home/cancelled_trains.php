<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cancel Train Status</title>
    <?php $this->load->view("include/header") ?>
</head>

<body>
    
    <?php $this->load->view("include/nav") ?>
    <div class="container custom-container">
        <ul class="breadcrumb">
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/" itemprop="url"><span itemprop="title">Indiaruler</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/cancelled-trains" itemprop="url"><span itemprop="title">Cancelled Trains</span></a></li>
        </ul>
        <h3>Cancelled Trains</h3>
        <p>Here, you can find the list of trains cancelled today. 
There shall be daily automatic updates of the trains and their respective details, and users have the option to search the cancelled trains by the dates they want to look for as well.</p>
        <form class="form-inline" method="post" action="">
            <div class="form-group">
                <label>Date</label>
                <select class="form-control" name="date" id="date"></select>
            </div>
            <div class="form-group">
                <button class="btn btn-info">Check</button>
            </div>
        </form>
        <p></p>
        <?php if (isset($data['trains'])):?>
        <p><strong>Total Cancelled Trains: <?php echo count($data['trains']);?></strong></p>
        <p><strong>Last Updated: <?php echo date('M d, Y g:i A', strtotime($data['last_updated']['date'] . " " . $data['last_updated']['time']));?></strong></p>
        <p></p>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-condesed table-striped" id="listTable">
                    <thead>
                        <th>Train Number</th>
                        <th>Train Name</th>
                        <th>Start Time</th>
                        <th>Destination Station</th>
                        <th>Source Station</th>
                    </thead>
                    <tbody>
                        <?php foreach ($data['trains'] as $value) { ?>
                        <tr>
                            <td><?php echo $value['train']['number']; ?></td>
                            <td><?php echo $value['train']['name']; ?></td>
                            <td><?php echo $value['train']['start_time']; ?></td>
                            <td><?php echo $value['dest']['name']. " - " . $value['dest']['code']; ?></td>
                            <td><?php echo $value['source']['name']. " - " .$value['source']['code']; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <?php $this->load->view('include/footer'); ?>
    <script type="text/javascript">
        
        /** Prepare dynamic dropdown menu */ 
        var dropdownStr = "";
        
        var yesterday = moment().subtract(1, "days").format("DD-MM-YYYY");
        var today = moment().format("DD-MM-YYYY");
        var tomorrow = moment().add(1, "days").format("DD-MM-YYYY");
        var selected = <?php if(isset($date)) { echo "'" . $date . "'"; } else { echo "''"; }?>;

        dropdownStr += "<option value='" + yesterday + "'";
        if(selected && selected === yesterday) {
            dropdownStr += "selected='selected'";
        }
        dropdownStr += ">" + yesterday + " (Yesterday)</option>";
        
        dropdownStr += "<option value='" + today + "'";
        if(!selected || selected === today) {
            dropdownStr += "selected='selected'";
        }
        dropdownStr += ">" + today + " (Today)</option>";
        
        dropdownStr += "<option value='" + tomorrow + "'";
        if(selected && selected === tomorrow) {
            dropdownStr += "selected='selected'";
        }
        dropdownStr += ">" + tomorrow + " (Tomorrow)</option>";

        // Append dropdown options to dropdown
        document.getElementById("date").innerHTML = dropdownStr;

        /** Initialize data table */
        $(document).ready(function(){
            $('#listTable').DataTable({responsive: true});
        });

    </script>
</body>

</html>
