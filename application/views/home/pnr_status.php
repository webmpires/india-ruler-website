<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>PNR Status</title>
    <?php $this->load->view("include/header") ?>
</head>

<body>
    
    <?php $this->load->view("include/nav") ?>
    <div class="container custom-container">
        <ul class="breadcrumb">
            <li itemscope itemtype="http://schema.org/ListItem"><a href="https://indiaruler.com/" itemprop="url"><span itemprop="title">Indiaruler</span></a></li>
            <li itemscope itemtype="http://schema.org/ListItem"><a href="https://indiaruler.com/pnr-status" itemprop="url"><span itemprop="title">Pnr Status</span></a></li>
        </ul>
        <h3>PNR Status</h3>
        <div class="row">
            <div class="col-md-6">
                <form class="form-inline" method="post" action="">
                    <div class="form-group">
                        <input type="number" class="form-control" name="pnr_number" placeholder="Enter PNR Number" maxlength="10" required>
                    </div>
                    <button class="btn btn-info">Get PNR Status</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?php if(isset($error) && $error == 1) { ?>
                    <p></p>
                    <div class="alert alert-danger">Error when getting data.</div>
                <?php } else if(isset($pnr)) { ?>
                <br>
                    <table class="table table-bordered table-condensed table-striped">
                        <tbody>
                            <tr>
                                <td>PNR</td>
                                <td><?php echo $pnr;?></td>
                            </tr>
                            <tr>
                                <td>Date Of Journey</td>
                                <td><?php echo $doj;?></td>
                            </tr>
                            <tr>
                                <td>From</td>
                                <td><?php echo $from_station['name'].'['.$from_station['code'].']';?></td>
                            </tr>
                            <tr>
                                <td>To</td>
                                <td><?php echo $to_station['name'].'['.$from_station['code'].']';?></td>
                            </tr>
                            <tr>
                                <td>Train Number</td>
                                <td><?php echo $train_num; ?></td>
                            </tr>
                            <tr>
                                <td>Train Name</td>
                                <td><?php echo $train_name;?></td>
                            </tr>
                            <tr>
                                <td>Booking Status</td>
                                <td><?php echo $passengers[0]['booking_status']; ?></td>
                            </tr>
                            <tr>
                                <td>Current Status</td>
                                <td><?php echo $passengers[0]['current_status']; ?></td>
                            </tr>
                            <tr>
                                <td>Chart Prepared</td>
                                <td><?php echo $chart_prepared; ?></td>
                            </tr>
                            <tr>
                                <td>Class</td>
                                <td><?php echo $class; ?></td>
                            </tr>
                            <tr>
                                <td>Total Passengers</td>
                                <td><?php echo $total_passengers; ?></td>
                            </tr>
                        </tbody>
                    </table>
                <?php    } ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.table').dataTable({searching: false, paging: false, responsive: true});
        });
    </script>
    <?php $this->load->view("include/footer"); ?>
</body>

</html>
