<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Live Train Running status | Spot your train status on Map</title>
    <?php $this->load->view("include/header") ?>
    <style type="text/css">
        .highlightRow {
            background: chartreuse !important;
        }
    </style>
    <!-- India ruler widget -->
    <!-- <script type="text/javascript">
        (function() {
          var oScript = document.createElement('script');
          oScript.type = 'text/javascript';
          oScript.async = true;
          oScript.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'indiaruler.net/assets/dist/widget.js';
          var oParent = document.getElementsByTagName('script')[0];
          oParent.parentNode.insertBefore(oScript, oParent);
        })();
    </script> -->
</head>

<body>
    
    <?php $this->load->view("include/nav") ?>
    <div class="container custom-container">
        <h1 class="m0">Live Train Status</h1>
        <?php if (isset($data["current_station"])) { ?>
        <ul class="breadcrumb" >
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/" itemprop="url"><span itemprop="title">Indiaruler</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/train-running-status" itemprop="url"><span itemprop="title">Train Running status</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb" class="active"><a href="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" itemprop="url"><span itemprop="title"><?php echo $train_number."-".strtoupper($train_name)?></span></a></li>
        </ul>
        <?php } else { ?>
            <ul class="breadcrumb" >
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/" itemprop="url"><span itemprop="title">Indiaruler</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/train-running-status" itemprop="url"><span itemprop="title">Train Running status</span></a></li>
        </ul>
        <p>Live train status is the feature that enable users to track down the real time location of trains.
Using this feature, you can find out time-wise schedule of the train which enable users to determine the approximate location of a train at a particular time. This brings you some of the simplest ways to trace down location of the train and comes quite handy to have an idea if your train will be on time or if there is any considerable delay.
Simply enter the train number to get the live train status. You will be provided with details such as name of each station, arrival and departure times and intervals on each station respectively and distance between each stop. You have the option to spot your train on map as well.</p>
        <?php } ?>
        <form class="form-inline" method="post" action="" id="train-form">
            <div class="form-group">
                <input type="text" class="form-control" name="train_number" placeholder="Train Number" id="trainnumber" required <?php if(isset($train_number)) {echo "value='$train_number'";}?>>
            </div>
            <div class="form-group">
                <select class="form-control" name="date" id="date"></select>
            </div>
            <button class="btn btn-info">Go</button>
        </form>
        <br>
        <?php if (isset($data["current_station"]) && isset($data['current_station']['station_'])) { ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info current-status-panel">
                    <div class="panel-heading"><h4>
                    <?php echo strtoupper($train_name) . " (" .$train_number.")";?> Running Train Status</h4></div>
                    <div class="panel-body">
                        <!-- <p>Showing Live running train status of <?php echo $train_name;?> (<?php echo $train_number;?>) for <?php echo date("jS F Y", strtotime($date));?>.<br> Currently <?php echo $train_name . " " . $data['position']; ?><br>
                        <?php if($data['current_station']['has_arrived'] == 1) {
                                echo "Current Station is ". $data['current_station']['station_']['name'];
                            } else if($data['current_station']['has_departed'] === 1) {
                                echo "Departed from ". $data['current_station']['station_']['name'];
                            } ?>
                        </p> -->
                         <p><?php echo strtoupper($train_name) . " (" . $train_number;?>) live status for <?php echo date("jS F Y", strtotime($date));?> is being displayed. 
                                <?php if($data['current_station']['has_departed']) {
                                    echo "Currently, " . $train_name . " (" . $train_number . ") has departed from " . $data['current_station']['station_']['name'] . ".";
                                };?> 
                             It is late by 
                             <span 
                                 <?php if($data['current_station']['status'] !== "0 Mins late") { ?>
                                    style="color: red" 
                                <?php } ?>>
                                <?php echo preg_replace("/ late/", "", $data['current_station']['status']); ?>.
                            </span>
                             <?php if($data['current_station']['has_arrived'] == 1) {
                                    echo "Current Station is <span style='color: limegreen;'>". $data['current_station']['station_']['name'] . "</span> ";
                                    if(!empty($nextStation) && $inconsistent == false) {
                                        echo "and the next station where train is to arrive is <span style='color: limegreen;'>" . $nextStation['station_']['name'] . "</span>.";
                                    }
                             }?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php } elseif((isset($data['current_station']) && !isset($data['current_station']['station_'])) || isset($_GET['error'])) { ?>
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Could not find any data.
            </div>
        <?php } ?>
        <br>
        <?php if (isset($data['route']) && count($data['route']) > 0):?>
            <div class="row">
                <div class="col-md-12">
                    <h3 class="m0">Train Route</h3>
                    <table class="table table-bordered table-condensed table-striped">
                        <thead>
                            <th class="text-center">Station</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Arrival(Actual)</th>
                            <th class="text-center">Depature(Actual)</th>
                            <th class="text-center">Schedule Arrival</th>
                            <th class="text-center">Schedule Depature</th>
                        </thead>
                        <tbody>
                            <?php $i = 0;
                                $data['current_station']['station_']['code'];

                                /*for ($j = 0; $j < count($data['route']); $j++) {
                                    // determine next station
                                    if($data['route'][$j]['station_']['code'] == $data['current_station']['station_']['code']) {
                                        $nextStationIdx = $j;
                                    }
                                }*/
                            
                            $isCurrentStationGone = false;

                            foreach ($data['route'] as $value) { ?>
                                <tr id="row<?php echo $i; ?>">
                                    <td>
                                        <?php
                                            $linkInfo = getLink('live_station', array("stationCode"=> $value['station_']['code'], "stationName" => $value['station_']['name']));
                                        ?>
                                        <a href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $value['station_']['name']; ?></a>
                                        <?php echo  " [" . $value['station_']['code'] . "]"; ?>
                                    </td>
                                    <td>
                                        <?php 
                                            echo $isCurrentStationGone ? '' : $value['status'];
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                            echo $isCurrentStationGone ? '' : ( $value['actarr_date'] . " " . $value['actarr']);
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                            echo $isCurrentStationGone ? '' : ( $value['actarr_date'] . " " . $value['actdep']);
                                       ?>
                                    </td>
                                    <td><?php echo $value['scharr_date'] . " " . $value['scharr'];?></td>
                                    <td><?php echo $value['scharr_date'] . " " . $value['schdep'];?></td>
                                </tr>
                           <?php $i++; 
                                
                                if($isCurrentStationGone === 0){
                                    $isCurrentStationGone = true;
                                } else if($value['station_']['code'] === $data['current_station']['station_']['code']){
                                    $isCurrentStationGone = 0;
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                    <p></p>
                    <h4>
                        Also Check:
                    </h4>
                    <p>
                        <?php $linkInfo = getLink('schedule', array("train_number"=> $train_number, "train_name" => $train_name));?> 
                        <a class="btn btn-info btn-interlink" href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>

                        <?php $linkInfo = getLink('seat_avail', array("train_number"=> $train_number, "train_name" => $train_name));?> 
                        <a class="btn btn-info btn-interlink" href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>

                        <?php $linkInfo = getLink('fare_inquiry', array("train_number"=> $train_number, "train_name" => $train_name));?> 
                        <a class="btn btn-info btn-interlink" href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>
                    </p>
                </div>
                <?php $this->load->view('include/map'); ?>
            </div>
        <?php endif ?>
    </div>

    <?php $this->load->view("include/footer"); ?>

    <script type="text/javascript">
        <?php if(isset($data['route']) && count($data['route']) > 0) { ?>
            var map, hasInstalledExtension = false, currentStatus, nextStation, currentStation;
            
            var currentStatus = "<?php echo $data['current_station']['station_']['code']; ?>";
            var trainDeparted = "<?php echo $data['current_station']['has_departed']; ?>";
            trainDeparted = trainDeparted && trainDeparted == 1;

            /** Master data array */
            var routes = <?php echo json_encode($data['route']);?>;
            var routeData = <?php echo json_encode($schedule_data['route']);?>;
                // console.log(routeData);
            for (var i = 0; i < routes.length; i++) {
                // determine next station
                if(routes[i].station_.code == currentStatus) {
                    currentStation = routes[i];
                    nextStation = routes[i + 1] || {};
                    nextStation.index = i + 1;
                }
            }

            // Filter route data
            routeData = routeData.filter(function(route){
                return route.lat !== 0 && route.lng !== 0;
            });

            // fetch source from data
            var source = routeData[0].fullname + ", " + routeData[0].state;

            // fetch destination from data
            var destination = routeData[routeData.length - 1].fullname + ", " + routeData[routeData.length - 1].state;
            
            // prepare waypoints data array
            var waypts = [];
            for (var i = 1; i < routeData.length - 1; i++) {
                waypts.push({
                  location: routeData[i].fullname + ", " + routeData[i].state,
                  stopover: true
                });
            }

            // console.log("SOURCE >>> ", source, "DESTINATION >>> ", destination, " WAY POINTS >>> ", waypts);

            function initMap() {

                // var directionsService = new google.maps.DirectionsService;
                // var directionsDisplay = new google.maps.DirectionsRenderer;
                
                var el = hasInstalledExtension ? document.getElementById('map2') : document.getElementById('map');

                map = new google.maps.Map(el, {
                  center: {lat: 22.6098, lng: 77.7694},
                  zoom: 6,
                  scrollwheel: false,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var positions = [],
                markers = [],
                infowindow = [],
                destIndex = routeData.length - 1;

                var min = [180, 180],
                    max = [-180, -180];
                
                var len = routeData.length;
                
                var icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==',
                    origin =  "<?php echo base_url(); ?>assets/images/origin.png",
                    destination =  "<?php echo base_url(); ?>assets/images/destination.png",
                    route =  "<?php echo base_url(); ?>assets/images/route.png", 
                    markerImage = null;

                for (var i = 0; i < len; i++) {
                    
                    var station = routeData[i];
                    
                    if (!station.lat || !station.lng) {
                        continue; 
                    }

                    positions.push(new google.maps.LatLng(station.lat, station.lng));
                    
                    min[0] = Math.min(min[0], station.lat);
                    min[1] = Math.min(min[1], station.lng);
                    max[0] = Math.max(max[0], station.lat);
                    max[1] = Math.max(max[1], station.lng);
                    
                    
                    var icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=='; 
                    
                    markerImage = null;

                    if(i === 0) {
                        markerImage = origin;
                    } else if(i === len - 1){
                        markerImage = destination;
                    } else {
                        markerImage = route;
                    }

                    markers.push(new google.maps.Marker({
                        position: positions[i],
                        map: map,
                        title: station.fullname + ' (' + station.code + ')',
                        icon: markerImage
                    }));

                    (function  (i) {

                        google.maps.event.addListener(markers[i], 'mouseover', function() {
                             infowindow[i] = new google.maps.InfoWindow({content: routeData[i].fullname });
                             infowindow[i].open(map, markers[i]);
                        });

                        google.maps.event.addListener(markers[i], 'mouseout', function() {
                             infowindow[i].close();
                        });
                        
                        if(currentStatus == routeData[i]['code']) {
                            markers[i].setAnimation(google.maps.Animation.BOUNCE);

                            markers[i].setIcon('https://maps.google.com/mapfiles/ms/icons/green-dot.png')
                        }

                    })(i);

                    // console.log(station.lat, station.lng, station.fullname, station.code);
                }

                console.log(markers.length);

                map.setCenter(new google.maps.LatLng(((max[0] + min[0]) / 2.0), ((max[1] + min[1]) / 2.0)));
                map.fitBounds(new google.maps.LatLngBounds(new google.maps.LatLng(min[0], min[1]), new google.maps.LatLng(max[0], max[1])));
                

                var trainRoute = new google.maps.Polyline({ map: map, path: positions, strokeColor: '#0070B1', strokeOpacity: 0.6, strokeWeight: 5 });

            }

            function detectExtension(extensionId, callback) { 
                    var img; 
                    img = new Image(); 
                    img.src = "chrome-extension://" + extensionId + "/images/icon.png"; 
                    img.onload = function() { 
                        callback(true); 
                    }; 
                    img.onerror = function() { 
                        callback(false); 
                    }; 
            }

            function showMap() {
                /** If mobile device then always show map */
                if(isMobile()) {
                    $("#blurry").remove();
                    hasInstalledExtension = true; // assume ext is installed
                    var intr = setInterval(function () {
                            if(google) {
                                initMap();
                                clearInterval(intr);
                            }
                        }, 1000);
                } else {
                    detectExtension('ffnhfeiahlmjjkonmpfcipljofhgnkli', function(res){
                         if(res) {
                            hasInstalledExtension = true;
                            $("#blurry").remove();
                         } else {
                            $("#nonBlurry").remove();
                         }
                        var intr = setInterval(function () {
                            if(google) {
                                initMap();
                                clearInterval(intr);
                            }
                        }, 1000);
                    });
                }

                
                /** Check if use has extension installed */

            }

            // function installExt() {
            //     chrome.webstore.install("https://chrome.google.com/webstore/detail/ffnhfeiahlmjjkonmpfcipljofhgnkli", 
            //         function (r) {
            //             console.log(r);
            //         }, function (e) {
            //             console.log(e);
            //         });
            // }

            showMap();
            $(document).ready(function() {

                var table = $('.table').dataTable({
                    searching: false, 
                    paging: false, 
                    responsive: true, 
                    "aaSorting": [],
                    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        
                        // if (aData[0].match(currentStatus) && !trainDeparted){
                        //     $(nRow).addClass('highlightRow');
                        // }

                        if (aData[0].indexOf('['+ (currentStatus || '') +']') !== -1 && !trainDeparted){
                            $(nRow).addClass('highlightRow');
                        }

                        if(nextStation && iDisplayIndex == nextStation.index && trainDeparted) {
                            $("td:nth-child(2)", nRow).html("<marquee direction=\"right\">Not reached yet</marquee>");
                            $("td:nth-child(3)", nRow).html("--");
                            $("td:nth-child(4)", nRow).html("--");
                            // aData[1] = "Not eached yet";
                            // aData[2] = "--";
                            // aData[3] = "--";
                        }
                    }
                });

                var trainInfo = '<?php echo $train_number . " " . $train_name; ?>';

                var url = trainInfo.replace(/\s+$/g, "").split(' ').join('-').toLowerCase() + '-timings';
                $("#scheduleLink").attr('href', window.location.origin + '/train-schedule/' + url);
            }); 

        <?php } ?>
            function isSelected(date) {
                var selected = '<?php if(isset($dateSlug)) {echo $dateSlug;} ?>';
                if(selected === date) {
                    return "selected";
                } else {
                    return '';
                }
            }
            $(document).ready(function() {

                /** Prepare dynamic dropdown menu */ 
                var dropdownStr = "";
                
                for (var i = 4; i >= -1; i--) {
                    var day = moment().subtract(i, "days").format("DD-MM-YYYY");
                    if(i == 1) {
                        dropdownStr += "<option value='yesterday' " + isSelected('yesterday') + ">" + day +" (Yesterday)</option>";
                        // dropdownStr += "<option value='yesterday'>" + day +" (Yesterday)</option>";
                    } else if (i == 0) {
                        dropdownStr += "<option value='' " + isSelected('') + ">" + day +" (Today)</option>";
                        // dropdownStr += "<option value='' selected>" + day +" (Today)</option>";
                    } else if (i == -1) {
                        dropdownStr += "<option value='tomorrow' " + isSelected('tomorrow') + ">" + day +" (Tomorrow)</option>";
                        // dropdownStr += "<option value='tomorrow'>" + day +" (Tomorrow)</option>";
                    } else {
                        dropdownStr += "<option value='"+ i +"daysago'" + isSelected(i +"daysago") + ">" + day +"</option>";
                        // dropdownStr += "<option value='"+ i +"daysago'>" + day +"</option>";
                    }
                }

                // var yesterday = moment().subtract(1, "days").format("DD-MM-YYYY");
                // var today = moment().format("DD-MM-YYYY");
                
                // dropdownStr += "<option value='" + yesterday + "'";

                // dropdownStr += ">" + yesterday + " (Yesterday)</option>";
                
                // dropdownStr += "<option value='" + today + "'";
                
                // Append dropdown options to dropdown
                document.getElementById("date").innerHTML = dropdownStr;

                $("#train-form").on('submit', function(e){
                    e.preventDefault();
                    
                    var trainnumber = $("#trainnumber").val();

                    if(trainnumber.match(/\d+/g)[0].length === trainnumber.length){
                        trainnumber = availableTags.filter(function(trainName){
                            return trainName.indexOf(trainnumber) !== -1;
                        })[0];
                    }

                    var statusdate = $("#date").val();

                    var url = trainnumber.replace(/\D+/g, '') + (statusdate ? ('?date=' + statusdate) : '');
                    
                    window.location.href = window.location.origin + '/train-running-status/' + url;

                    // $(this).submit();
                });
            });
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBE_JyCcLx6Y75jvuqDjkveRgSgrlpyIk4"></script>
</body>

</html>