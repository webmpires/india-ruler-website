<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Train Between Station</title>
    <?php $this->load->view("include/header") ?>
</head>
<body>
    <?php $this->load->view("include/nav") ?>
	<div class="container custom-container">
        <?php if (isset($train) && count($train) > 0) { ?>
        <ul class="breadcrumb">
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/" itemprop="url"><span itemprop="title">Indiaruler</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/trains-between-stations" itemprop="url"><span itemprop="title">Trains between Stations</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb" class="active"><a href="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" itemprop="url"><span itemprop="title"><?php echo $sourceName . " TO " . $destName;?></span></a></li>
        </ul>
        <?php } else { ?>
        <ul class="breadcrumb">
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/" itemprop="url"><span itemprop="title">Indiaruler</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/trains-between-stations" itemprop="url"><span itemprop="title">Trains between Stations</span></a></li>
        </ul>
        <?php } ?>
		<h3>Train Between Station</h3>
        <p>Trains between stations is the feature which enlists all those trains that run between any two stations mentioned by the user.
With this feature, you can find all the trains running between any two railway stations. Simply enter the destinations and get the details and timings of different trains covering those destinations.
</p>
		<form class="form-inline"  method="post" action="" id="train-form">
			<div class="form-group">
				<input type="text" class="form-control" name="source" placeholder="Source Station" id="source">
			</div>
			<div class="form-group">
				<input type="text" class="form-control" name="destination" placeholder="Destination Name" id="dest">
			</div>
			<div class="form-group"><button class="btn btn-info">Go</button></div>
		</form>
        <br>
	   <?php if (isset($train) && count($train) > 0) { ?>
        <div class="row">
            <div class="col-md-12">
                <table id="listTbl" class="table table-bordered table-condesed table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">Train Number</th>
                            <th class="text-center">Train Name</th>
                            <th class="text-center">Source</th>
                            <th class="text-center">Depature</th>
                            <th class="text-center">Destination</th>
                            <th class="text-center">Arrival</th>
                            <th>
                                <table class="table table-bordered table-condensed">
                                    <thead>
                                        <tr>
                                            <td colspan="7" class="text-center">Day Run</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>&nbsp;&nbsp;M&nbsp;&nbsp;</td>
                                            <td>&nbsp;&nbsp;T&nbsp;&nbsp;</td>
                                            <td>&nbsp;&nbsp;W&nbsp;&nbsp;</td>
                                            <td>&nbsp;&nbsp;T&nbsp;&nbsp;</td>
                                            <td>&nbsp;&nbsp;F&nbsp;&nbsp;</td>
                                            <td>&nbsp;&nbsp;S&nbsp;&nbsp;</td>
                                            <td>&nbsp;&nbsp;S&nbsp;&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </th>
                            <th>
                                <table class="table table-bordered table-condensed">
                                    <thead>
                                        <tr>
                                            <td colspan="7" class="text-center">Classes</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1A</td>
                                            <td>2A</td>
                                            <td>3A</td>
                                            <td>CC</td>
                                            <td>SS</td>
                                            <td>2S</td>
                                            <td>3E</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($train as $value) { ?>
                            <tr>
                                <td>
                                    <?php
                                        $linkInfo = getLink('schedule', array("train_number"=> $value['number'], "train_name" => $value['name']));
                                    ?>
                                    <a href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $value['number']; ?></a>
                                </td>
                                <td><?php echo $value['name'];?></td>
                                <td>
                                    <?php
                                        $linkInfo = getLink('live_station', array("stationCode"=> $value['from']['code'], "stationName" => $value['from']['name']));
                                    ?>
                                    <a href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $value['from']['name'] ?></a>
                                </td>
                                <td><?php echo $value['src_departure_time'];?></td>
                                <td>
                                    <?php
                                        $linkInfo = getLink('live_station', array("stationCode"=> $value['to']['code'], "stationName" => $value['to']['name']));
                                    ?>
                                    <a href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $value['to']['name'] ?></a>
                                </td>
                                <td><?php echo $value['dest_arrival_time'];?></td>
                                <td>
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <?php foreach ($value['days'] as $va) {
                                                    echo "<td>" . $va['runs'] . "</td>";
                                                }?>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <?php foreach ($value['classes'] as $v) {
                                                    echo "<td>" . $v['available'] . '</td>';
                                                }?>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p>
                                        <?php
                                            $train_number = $value['number'];
                                            $train_name = $value['name'];
                                            $linkInfo = getLink('schedule', array("train_number"=> $train_number, "train_name" => $train_name));?> 
                                        <a class="btn btn-info btn-sm btn-block" href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>
                                        <br>
                                        <?php $linkInfo = getLink('seat_avail', array("train_number"=> $train_number, "train_name" => $train_name));?> 
                                        <a class="btn btn-info btn-sm btn-block" href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>
                                        <br>
                                        <?php $linkInfo = getLink('fare_inquiry', array("train_number"=> $train_number, "train_name" => $train_name));?> 
                                        <a class="btn btn-info btn-sm btn-block" href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>
                                    </p>
                                </td>
                            </tr>
                       <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif(isset($train) && count($train) == 0) { ?>
        <div class="alert alert-dismissible alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            Could not find any data.
        </div>
    <?php } ?>
    </div>
    <?php $this->load->view('include/footer'); ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#train-form").on('submit', function(e){
                e.preventDefault();

                var src, dest, tmpSrc, srcCode, tmpDest, destCode, url;
                
                src = $("#source").val();
                dest = $("#dest").val();
                
                // get source code
                tmpSrc = src.split(' - ');
                srcCode = tmpSrc[tmpSrc.length - 1];

                // get dest code
                tmpDest = dest.split(' - ');
                destCode = tmpDest[tmpDest.length - 1];

                url = tmpSrc[0].split(' ').join('-').toLowerCase() + '-to-' + tmpDest[0].split(' ').join('-').toLowerCase() + '/' + srcCode + '-' + destCode;
                
                window.location.href = window.location.origin + '/trains-between-stations/' + url;

                // $(this).submit();
            });
            
            $('#listTbl').dataTable({
                searching: false,
                paging: false,
                responsive: true
            });

        });
    </script>
</body>

</html>
