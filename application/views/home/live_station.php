<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Live Station</title>
    <?php $this->load->view("include/header") ?>
</head>
<body>
	
    <?php $this->load->view("include/nav") ?>
    <div class="container custom-container">
        <?php if (isset($total) && isset($train) && count($train) > 0) { ?>
        <ul class="breadcrumb">
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/" itemprop="url"><span itemprop="title">Indiaruler</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/railway-station" itemprop="url"><span itemprop="title">Railway Station</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb" class="active"><a href="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" itemprop="url"><span itemprop="title"><?php echo $name;?></span></a></li>
        </ul>
        <?php } else { ?>
            <ul class="breadcrumb">
                <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/" itemprop="url"><span itemprop="title">Indiaruler</span></a></li>
                <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/railway-station" itemprop="url"><span itemprop="title">Railway Station</span></a></li>
            </ul>
        <?php } ?>
        <h3>Live Station</h3>
        <p>Live station is the feature which the user can use to obtain the list of trains that shall be arriving at a railway station of his choice at the duration selected by him.
The user need to enter the station he want to search and select the duration (for eg: 2 hours). The list of all the trains that are expected to arrive at that particular railway station in 2 hours shall be displayed to the user then.
</p>
        <div class="row">
            <div class="col-md-12">
                <form class="form-inline" method="post" action="" id="train-form">
                    <div class="form-group">
                        <input type="text" class="form-control" name="source" id="source" placeholder="Source Station">
                    </div>
                    <div class="form-group">
                        <select name="hours" class="form-control" id="hours">
                            <option value="today">Today</option>
                            <option value="2hours">2 Hours</option>
                            <option value="4hours">4 Hours</option>
                        </select>
                    </div>
                    <button class="btn btn-info">Go</button>
                </form>
            </div>
        </div>
        <?php if(isset($total) && isset($train) && count($train) > 0) {?>
        <div class="row">
            <div class="col-md-12">
                <p></p>
                <table class="table table-bordered table-condensed table-striped">
                    <thead>
                        <th class="text-center">Train Number</th>
                        <th class="text-center">Train Name</th>
                        <th class="text-center">Expected Arrival</th>
                        <th class="text-center">Delay In Arrival</th>
                        <th class="text-center">Expected Departure</th>
                        <th class="text-center">Delay In Departure</th>
                    </thead>
                    <tbody>
                        <?php foreach ($train as $value) { ?>
                            <tr>
                                <td>
                                    <?php $linkInfo = getLink('schedule', array("train_number"=> $value['number'], "train_name" => $value['name']));?> 
                                    <a href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $value['number']; ?></a>
                                </td>
                                <td><?php echo $value['name']; ?></td>
                                <td><?php echo $value['scharr']; ?></td>
                                <td><?php echo $value['delayarr']; ?></td>
                                <td><?php echo $value['schdep']; ?></td>
                                <td><?php echo $value['delaydep']; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php } elseif(isset($train) && count($train) == 0) { ?>
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Could not find any data.
            </div>
        <?php } ?>
    </div>
    <?php $this->load->view('include/footer'); ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.table').dataTable({searching: false, paging: false, responsive: true, aaSorting: [[2, 'asc']]});
            
            $("#train-form").on('submit', function(e){
                e.preventDefault();
                
                var stationName = $("#source").val();
                var tmp = stationName.split(' - ');
                stationName = tmp[0];
                var stationCode = tmp[tmp.length - 1];
                var time = $("#hours").val();

                var url = stationCode.toLowerCase() + '-' + stationName.replace(' -', '').split(' ').join('-').toLowerCase();

                if(time !== 'today') {
                    url += "?time=" + time;
                }
                
                window.location.href = window.location.origin + '/railway-station/' + url;

                // $(this).submit();
            });
        });
    </script>
</body>

</html>

