<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <?php $this->load->view("include/header") ?>
</head>

<body>
    
    <?php $this->load->view("include/nav") ?>
    <div class="container custom-container">
        <?php if (isset($availability) && count($availability) > 0) { ?>
        	
        <div class="row">
        	<div class="col-md-12">
        		<table class="table table-bordered table-condesed table-striped">
        			<thead>
        				<th class="text-center">Train Number</th>
        				<th class="text-center">Train Name</th>
        				<th class="text-center">Date</th>
        				<th class="text-center">Source</th>
        				<th class="text-center">Destination</th>
        				<th class="text-center">Class</th>
        				<th class="text-center">Quota</th>
        				<th class="text-center">Availability</th>
        			</thead>
        			<tbody>
        				<?php foreach ($availability as $value) { ?>
        					<tr>
        						<td><?php echo $train['number'];?></td>
        						<td><?php echo $train['name'];?></td>
        						<td><?php echo $value['date'];?></td>
        						<td><?php echo $from['name'];?></td>
        						<td><?php echo $to['name'];?></td>
        						<td><?php echo $class['name'];?></td>
        						<td><?php echo $quota['quota_name'];?></td>
                                <td><?php echo $value['status'];?></td>
        					</tr>
        				<?php }?>
        			</tbody>
        		</table>
        	</div>
        </div>
        <?php } else { ?>
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Could not find any data.
            </div>
        <?php } ?>
    </div>
    <?php $this->load->view('include/footer'); ?>
</body>

</html>
