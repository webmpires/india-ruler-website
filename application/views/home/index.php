<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Indiaruler</title>

    <?php $this->load->view("include/header") ?>
  	<?php $this->load->view("include/nav") ?>
  </head>

  <body>

    <div class="container custom-container">
      <div class="row">
        <div class="col-md-12">
          <h4>Dealing with train travel (IR) was never this simple!</h4>
          <p>Rail transport is a significant part of overall transportation of Indian transportation network. Since IR (Indian railways) offer affordable comfort in transport, majority of people choose railways for long distance domestic journeys. We, at India ruler have developed this site with the sole aim of making the rail journey even more comfortable. We have developed these real-time, efficient features and services to accomplish the same.</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="panel panel-info">
            <div class="panel-body">Along with the ease of PNR status tracking using our extension, several beneficial features are made available for you free of cost here. The features include checking train set availability, getting real time train status, fare enquiries, train schedules, trains between stations and cancelled trains. <a href="https://chrome.google.com/webstore/detail/pnr-status/ffnhfeiahlmjjkonmpfcipljofhgnkli?hl=en" target="_blank">Install our extension here</a></div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-info">
            <div class="panel-heading">Live Train status</div>
            <div class="panel-body">The feature of Live train brings you some of the easiest ways to trace down the real time location of the trains using GPS. With this, you can more comfortably know the live location of trains. This is in particular helpful if you are awaiting the arrival of a guest.</div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-info">
            <div class="panel-heading">Train Seat availability</div>
            <div class="panel-body">This helps you keep track of or check the availability of seats on various trains. With it, you can check out if there are seats available in the train you are considering for travel. Wiser to make the booking soon if it is almost filled!</div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="panel panel-info">
            <div class="panel-heading">Fare inquiry</div>
            <div class="panel-body">This feature will help you find the on going or updated fares of different trains in Indian railways (IR).</div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-info">
            <div class="panel-heading">Trains between stations</div>
            <div class="panel-body">With this feature, you can find all the trains between any two main railway stations.</div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-info">
            <div class="panel-heading">Train schedule</div>
            <div class="panel-body">This feature enables you to easily get your hands on the train schedules- arrival and departure times for various stations etc.</div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <p>Most importantly, there is no bulkier information that you need to enter. You just need the basic information like train number or name, PNR numbers etc. with which you can make use of all these features. Our extension as well as site has been designed keeping in view utmost user-friendliness.</p>
          <p>Once in a while everyone gets stuck with wait-listed train tickets and checking them, hustle-bustle of travel preparations and daily works. Well, we can at least take a part of the burden off your shoulder. While you have our extension installed, it is our duty to update you with any progress in your PNR status.</p>
          <p>While it is just a matter of few minutes to install our extension on your browser and inputting your PNR number, the application and services offered here is sure to make travel things and dealing with IR ( Indian railways) pretty much easier for you.</p>
          <p>All the details provided to you can be counted upon as they are on the basis of direct information from authentic IRCTC sites. This can be your great travel companion.</p>
        </div>
      </div>
      </div>
  	</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
        <?php $this->load->view("include/footer") ?>
  </body>
</html>