<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Seat Availbility</title>
    <?php $this->load->view("include/header") ?>
</head>

<body>

<?php $this->load->view("include/nav") ?>
<div class="container custom-container">
    <h1>Seat Availbility</h1>
    <?php if (isset($data["train"]) && count($data["train"]) > 0) { ?>
        <ul class="breadcrumb">
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/"
                                                                               itemprop="url"><span itemprop="title">Indiaruler</span></a>
            </li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a
                    href="https://indiaruler.com/seat-availability" itemprop="url"><span itemprop="title">Seat Availability</span></a>
            </li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb" class="active"><a
                    href="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"
                    itemprop="url"><span itemprop="title"><?php
                        if (!$formData['searchByTrain']) {
                            echo $data["sourceName"] . " TO " . $data['destName'];
                        } else {
                            echo $data['train'][0]['number'] . " " . $data['train'][0]['name'];
                        }
                        ?></span></a></li>
        </ul>
        <?php if ($formData['searchByTrain']) {
            $classes = '';
            $i = 0;
            foreach ($data['train'][0]['classes'] as $v) {
                if ($v['available'] == "Y") {
                    if ($i !== 0) {
                        $classes .= " / ";
                    }
                    $classes .= $v['class-code'] . " ";
                    $i++;
                }
            }

            ?>
            <h1><?php echo $data['train'][0]['number'] . " " . $data['train'][0]['name']; ?> Seat Availbility</h1>
            <p><?php echo $data['train'][0]['number']; ?> seat availability is shown
                below. <?php echo $data['train'][0]['name']; ?>(<?php echo $data['train'][0]['number']; ?>)
                travelling from <?php echo $data['train'][0]['from']['name']; ?>
                to <?php echo $data['train'][0]['to']['name']; ?>, has seat availability in <?php echo $classes; ?>.
                Further details of seat
                availability available in respective class links..
            </p>
        <?php } else {
            $names = '';
            $i = 0;
            foreach ($data["train"] as $value) {
                if ($i !== 0) {
                    $names .= ", ";
                }
                $names .= $value['name'] . " ";
                $i++;
            }

            ?>
            <h1><?php echo $data["sourceName"] . " TO " . $data['destName']; ?> train Seat Availbility</h1>
            <p>Seat availability in trains traveling from <?php echo $data["sourceName"]; ?>
                to <?php echo $data["destName"]; ?> on <?php echo date('d M Y', strtotime($formData["date"])); ?>.
                <?php echo $names; ?> are the trains covering the stations of your preference. Further seat availability
                details in the respective class links.
            </p>
        <?php }
    } else { ?>
        <ul class="breadcrumb">
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/"
                                                                               itemprop="url"><span itemprop="title">Indiaruler</span></a>
            </li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a
                    href="https://indiaruler.com/seat-availability" itemprop="url"><span itemprop="title">Seat Availability</span></a>
            </li>
        </ul>
        <p>Train seat availability is the feature that list out the number of available seats in a train.
            This allows travellers to explore the availability of seats prior to booking on various trains. You can keep
            track of seats and figure out if the seats are fast filled.
            You can get the seat availability details date-wise, by entering the train number, travel date, category and
            quota. Available number of seats in each class (like sleeper, First AC, CC etc.) would be displayed to you.
        </p>
    <?php } ?>
    <div class="row">
        <div class="col-md-5">
            <h4>Search By Train Number</h4>

            <form class="form-horizontal" method="post" action="" id="search-by-train-form">
                <div class="form-group">
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="train_number" placeholder="Train Number"
                               id="trainnumber" maxlength="5" required>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-info btn-block">Go</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-6">
            <h4>Search By Stations</h4>

            <form class="form-horizontal" method="post" action="" id="train-form">
                <div class="form-group">
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="source" placeholder="Source Station" id="source"
                               maxlength="60" required>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="destination" placeholder="Destination Station"
                               id="dest" maxlength="60" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <input type="text" class="form-control dtPicker" name="date" id="date" placeholder="Date"
                               required>
                    </div>
                    <div class="col-md-4">
                        <select name="quota" class="form-control" id="quota">
                            <option value="GN">General Quota</option>
                            <option value="CK">Tatkal Quota</option>
                            <option value="PT">Premium Tatkal Quota</option>
                            <option value="DF">Defence Quota</option>
                            <option value="FT">Foreign Tourist</option>
                            <option value="DP">Duty Pass Quota</option>
                            <option value="HP">Handicaped Quota</option>
                            <option value="PH">Parliament House Quota</option>
                            <option value="SS">Lower Berth Quota</option>
                            <option value="YU">Yuva Quota</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-info btn-block">Go</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php if (isset($data["train"]) && count($data["train"]) > 0) { ?>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <table class="table table-bordered table-condensed table-striped" id="listTable">
                    <thead>
                    <th class="text-center">Train Number</th>
                    <th class="text-center">Train Name</th>
                    <th class="text-center">Source</th>
                    <th class="text-center">Departure</th>
                    <th class="text-center">Destination</th>
                    <th class="text-center">Arrival</th>
                    <th class="text-center">Classes(Click to check availability)</th>
                    <th>
                        <table class="table table-bordered table-condensed">
                            <thead>
                                <tr>
                                    <td colspan="7" class="text-center">Day Run</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>M</td>
                                    <td>T</td>
                                    <td>W</td>
                                    <td>T</td>
                                    <td>F</td>
                                    <td>S</td>
                                    <td>S</td>
                                </tr>
                            </tbody>
                        </table>
                    </th>
                    <?php if (isset($formData) && $formData['searchByTrain'] == false) { ?>
                    <th></th>
                    <?php } ?>
                    </thead>
                    <tbody>
                    <?php foreach ($data["train"] as $value) { ?>
                        <tr>
                            <td>
                                <?php $linkInfo = getLink('schedule', array("train_number" => $value['number'], "train_name" => $value['name'])); ?>
                                <a href="<?php echo $linkInfo['href']; ?>" target="_blank"
                                   title="<?php echo $linkInfo['title']; ?>"><?php echo $value['number']; ?></a>
                            </td>
                            <td>
                                <a href="<?php echo $linkInfo['href']; ?>" target="_blank"
                                   title="<?php echo $linkInfo['title']; ?>"><?php echo $value['name']; ?></a>        
                            </td>
                            <td>
                                <?php
                                $linkInfo = getLink('live_station', array("stationCode" => $value['from']['code'], "stationName" => $value['from']['name']));
                                ?>
                                <a href="<?php echo $linkInfo['href']; ?>" target="_blank"
                                   title="<?php echo $linkInfo['title']; ?>"><?php echo $value['from']['name']; ?></a>
                            </td>
                            <td><?php echo $value['src_departure_time']; ?></td>
                            <td>
                                <?php
                                $linkInfo = getLink('live_station', array("stationCode" => $value['to']['code'], "stationName" => $value['to']['name']));
                                ?>
                                <a href="<?php echo $linkInfo['href']; ?>" target="_blank"
                                   title="<?php echo $linkInfo['title']; ?>"><?php echo $value['to']['name']; ?></a>
                            </td>
                            <td><?php echo $value['dest_arrival_time']; ?></td>
                            <td><?php $i = 0;
                                foreach ($value['classes'] as $v) {
                                    if ($v['available'] == "Y") { ?>
                                        <?php
                                        if ($i !== 0) {
                                            echo " / ";
                                        }
                                        ?>
                                        <a href="<?php echo base_url(); ?>main/seat_avail_check?<?php
                                        $queryData = $formData;
                                        $queryData["class"] = $v['code'];
                                        $queryData["train_number"] = $value['number'];
                                        unset($queryData["searchByTrain"]);
                                        echo http_build_query($queryData); ?>
                                             ">
                                            <?php echo $v['code'] . " "; ?></a>
                                        <?php $i++;
                                    }
                                } ?></td>
                            <td>
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr><?php foreach ($value['days'] as $va) {
                                                echo "<td>" . $va['runs'] . "</td>";
                                            } ?></tr>
                                    </tbody>
                                </table>
                            </td>
                            <?php if (isset($formData) && $formData['searchByTrain'] == false) { ?>
                            <td>
                                <p>
                                    <?php
                                        $train_number = $value['number'];
                                        $train_name = $value['name'];
                                        $linkInfo = getLink('schedule', array("train_number"=> $train_number, "train_name" => $train_name));?> 
                                    <a class="btn btn-info btn-sm btn-block" href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>
                                    <br>
                                    <?php $linkInfo = getLink('running_status', array("train_number"=> $train_number, "train_name" => $train_name));?> 
                                    <a class="btn btn-info btn-sm btn-block" href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>
                                    <br>
                                    <?php $linkInfo = getLink('fare_inquiry', array("train_number"=> $train_number, "train_name" => $train_name));?> 
                                    <a class="btn btn-info btn-sm btn-block" href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>
                                </p>
                            </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <br>
                <?php if (isset($formData) && $formData['searchByTrain'] == true) { ?>
                    <p>
                        <?php
                        $train_number = $data['train'][0]['number'];
                        $train_name = $data['train'][0]['name'];

                        $linkInfo = getLink('running_status', array("train_number" => $train_number, "train_name" => $train_name)); ?>
                        <a class="btn btn-info btn-interlink" href="<?php echo $linkInfo['href']; ?>" target="_blank"
                           title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>

                        <?php $linkInfo = getLink('fare_inquiry', array("train_number" => $train_number, "train_name" => $train_name)); ?>
                        <a class="btn btn-info btn-interlink" href="<?php echo $linkInfo['href']; ?>" target="_blank"
                           title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>

                        <?php $linkInfo = getLink('schedule', array("train_number" => $train_number, "train_name" => $train_name)); ?>
                        <a class="btn btn-info btn-interlink" href="<?php echo $linkInfo['href']; ?>" target="_blank"
                           title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>
                    </p>
                <?php } ?>
                <div class="row">
                    <div class="col-md-6">
                        <h4>Classes of Accommodation</h4>
                        <table class="table table-bordered">
                            <tr>
                                <td>1A</td>
                                <td>First class Air-conditioned (also Executive Class)</td>
                            </tr>
                            <tr>
                                <td>2A</td>
                                <td>Air-conditioned 2-tier sleeper</td>
                            </tr>
                            <tr>
                                <td>FC</td>
                                <td>First Class</td>
                            </tr>
                            <tr>
                                <td>3A</td>
                                <td>Air-conditioned 3-tier sleeper</td>
                            </tr>
                            <tr>
                                <td>CC</td>
                                <td>Air-conditioned chair car</td>
                            </tr>
                            <tr>
                                <td>SL</td>
                                <td>Sleeper class</td>
                            </tr>
                            <tr>
                                <td>2S</td>
                                <td>Second-class Sitting</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } elseif (
        ((isset($data) && isset($data["train"]) && count($data["train"]) == 0))
        || isset($_GET['error'])
    ) { ?>
        <div class="alert alert-dismissible alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            Could not find any data.
        </div>
    <?php } ?>
</div>
<?php $this->load->view('include/footer'); ?>
<script type="text/javascript">
    /** Initialize data table */
    $(document).ready(function () {

        <?php if(isset($formData) && $formData['searchByTrain'] == false) { ?>
            $('#listTable').DataTable({responsive: true});
        <?php } elseif (isset($formData) && $formData['searchByTrain'] == true) { ?>
            $('#listTable').DataTable({responsive: true, paginate: false, bFilter: false, bInfo: false});
        <?php } ?>

        $("#search-by-train-form").on('submit', function (e) {
            e.preventDefault();

            var trainnumber = $("#trainnumber").val();

            if (trainnumber.match(/\d+/g)[0].length === trainnumber.length) {
                trainnumber = availableTags.filter(function (trainName) {
                    return trainName.indexOf(trainnumber) !== -1;
                })[0];
            }

            var url = trainnumber.replace(/\D+/g, '');

            window.location.href = window.location.origin + '/seat-availability/' + url;

            // $(this).submit();
        });

        $("#train-form").on('submit', function (e) {
            e.preventDefault();

            var src, dest, date, quota, tmpSrc, srcCode, tmpDest, destCode, url;

            src = $("#source").val();
            dest = $("#dest").val();
            date = $("#date").val();
            quota = $("#quota").val();

            // get source code
            tmpSrc = src.split(' - ');
            srcCode = tmpSrc[tmpSrc.length - 1];

            // get dest code
            tmpDest = dest.split(' - ');
            destCode = tmpDest[tmpDest.length - 1];

            url = tmpSrc[0].split(' ').join('-').toLowerCase() + '-to-' + tmpDest[0].split(' ').join('-').toLowerCase() + '/' + srcCode + '-' + destCode;

            if (date !== moment().format("YYYY-MM-DD") && quota === "GN") {
                url += "?date=" + date;
            } else if (date === moment().format("YYYY-MM-DD") && quota !== "GN") {
                url += "?quota=" + quota;
            } else if (date !== moment().format("YYYY-MM-DD") && quota !== "GN") {
                url += "?date=" + date + "&quota=" + quota;
            }

            window.location.href = window.location.origin + '/seat-availability/' + url;

        });
    });
</script>
</body>

</html>
