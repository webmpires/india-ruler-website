<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Fare Enquiry</title>
    <?php $this->load->view("include/header") ?>
    <style>
        [ng\:cloak],[ng-cloak],.ng-cloak{display:none !important}
    </style>
</head>

<body>
    
    <?php $this->load->view("include/nav") ?>
    <div class="container custom-container">
        <h1>Fare Enquiry</h1>
        <?php if (isset($quota)) { ?>
        <ul class="breadcrumb">
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/" itemprop="url"><span itemprop="title">Indiaruler</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/fare-enquiry" itemprop="url"><span itemprop="title">Fair Enquiry</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb" class="active"><a href="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" itemprop="url"><span itemprop="title"><?php echo $train['number'] . " " . $train['name'];?></span></a></li>
        </ul>
        <h3>Fare Enquiry</h3>
        <p>
            <?php echo $train['name'] . ", " . $train['number']; ?> travel fares for journey from <?php echo $from['name']; ?> to <?php echo $to['name']?> in 
            <?php 
                $str = '';
                foreach($fare as $f) {
                    $str .= $f['name'] . ", ";
                }
                echo rtrim($str, ', ');
            ?>
             for <?php echo $quota['name']; ?> is 
             <?php 
                $str = '';
                foreach($fare as $f) {
                    $str .= "&#8377;" . $f['fare'] . "/- , ";
                }
                echo rtrim($str, ', ');
            ?>
              respectively.
        </p>
        <?php } else { ?>
        <ul class="breadcrumb">
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/" itemprop="url"><span itemprop="title">Indiaruler</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/fare-enquiry" itemprop="url"><span itemprop="title">Fair Enquiry</span></a></li>
        </ul>
        <p>Fare enquiry feature is the one which presents to its users, the information about prices of train tickets of different classes.
This feature will help you find the on going or updated fares of different trains in Indian railways.
For the purpose of fare enquiry, you just need to enter the train and  travel dates you prefer, destination, and the category you want to travel in. You will be shown the travel fare as on the day. This shall provide you with the idea of your travel expenses, so that you can plan accordingly.
</p>
        <?php } ?>
        <div class="row" ng-app="indiaruler" ng-controller="formCtrl">
            <div class="col-md-9">
                <form class="form-horizontal" method="post" action="" ng-cloak id="train-form">
                    <div class="form-group">
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="train_number" id="trainnumber" placeholder="Train Number" required ng-model="data.train_number" ng-change="onTrainNumberChange()">
                        </div>
                        <div class="col-md-4" ng-if="!loadedData">
                            <a class="btn btn-info" href="javascript:void(0)" ng-click="loadData()" ng-disabled="inProgress">Go</a>
                        </div>
                        <div class="col-md-4" ng-if="loadedData">
                            <!-- <input type="text" class="form-control" name="source" id="source" placeholder="Source Station" required> -->
                            <select name="source" class="form-control" ng-model="data.source" id="source">
                                <option ng-selected="data.source === src.code" ng-repeat="src in scheduleData" value="{{src.code}}">{{src.station_name}}</option>
                            </select>
                        </div>
                        <div class="col-md-4" ng-if="loadedData">
                            <!-- <input type="text" class="form-control" name="dest" id="dest" placeholder="Destintion Station" required> -->
                            <select name="destination" class="form-control" ng-model="data.destination" id="dest">
                                <option ng-selected="data.destination === dest.code" ng-repeat="dest in destScheduleData" value="{{dest.code}}">{{dest.station_name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" ng-if="loadedData">
                        <div class="col-md-4">
                            <input type="text" placeholder="Date" class="form-control" name="date" required id="date" date-picker>
                        </div>
                        <div class="col-md-4">
                            <select name="age" class="form-control" id="age">
                                <option value="8">Child AGE[5-11]</option>
                                <option value="30" selected="selected">ADULT AGE[12 and Above]</option>
                                <option value="60">Senior Citizer Female Age[58 and above</option>
                                <option value="61">Senior Citizer male Age[60 and above</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select name="quota" class="form-control" id="quota">
                                <option value="GN">General Quota</option>
                                <option value="CK">Tatkal Quota</option>
                                <option value="PT">Premium Tatkal Quota</option>
                                <option value="DF">Defence Quota</option> 
                                <option value="FT">Foreign Tourist</option>
                                <option value="DP">Duty Pass Quota</option> 
                                <option value="HP">Handicaped Quota</option>
                                <option value="PH">Parliament House Quota</option>
                                <option value="SS">Lower Berth Quota</option>
                                <option value="YU">Yuva Quota</option> 
                            </select> 
                        </div> 
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <button class="btn btn-info" ng-if="loadedData">Check</button>
                        </div>
                    </div>
                </form>
            </div>  
        </div>
        <?php if(isset($quota)) {?>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-condensed table-striped">
                    <thead>
                        <th class="text-center">Train Number</th>
                        <th class="text-center">Train Name</th>
                        <th class="text-center">Source</th>
                        <th class="text-center">Destination</th>
                        <th class="text-center">Fare</th>
                        <th class="text-center">Type</th>
                        <th class="text-center">Quota</th>
                    </thead>
                    <tbody>
                        <?php foreach ($fare as $value) { ?>
                            <tr>
                                <td>
                                    <?php $linkInfo = getLink('schedule', array("train_number"=> $train['number'], "train_name" => $train['name']));?> 
                                    <a href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $train['number']; ?></a>
                                </td>
                                <td><?php echo $train['name']; ?></td>
                                <td>
                                    <?php
                                        $linkInfo = getLink('live_station', array("stationCode"=> $from['code'], "stationName" => $from['name']));
                                    ?>
                                    <a href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $from['name']; ?></a>
                                </td>
                                <td>
                                    <?php
                                        $linkInfo = getLink('live_station', array("stationCode"=> $to['code'], "stationName" => $to['name']));
                                    ?>
                                    <a href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $to['name']; ?></a>
                                </td>
                                <td>&#8377;<?php echo $value['fare']; ?> /-</td>
                                <td><?php echo $value['name']; ?></td>
                                <td><?php echo $quota['name']; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <p></p>
                <h4>
                    Also Check:
                </h4>
                <p>
                <?php 
                    $train_number = $train['number'];
                    $train_name = $train['name'];       
                    $linkInfo = getLink('running_status', array("train_number"=> $train_number, "train_name" => $train_name));?> 
                    <a class="btn btn-info" href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>

                    <?php $linkInfo = getLink('seat_avail', array("train_number"=> $train_number, "train_name" => $train_name));?> 
                    <a class="btn btn-info" href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>
                </p>
            </div>
        </div>
        <?php } elseif(isset($fare) && count($fare) == 0) { ?>
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Could not find any data.
            </div>
        <?php } ?>
    </div>
    <?php $this->load->view('include/footer'); ?>
    <script type="text/javascript" src="https://code.angularjs.org/1.3.17/angular.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/dist/app.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.table').dataTable({searching: false, paging: false, responsive: true});
        });
    </script>
</body>

</html>
