<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>
        <?php if (isset($route) && count($route) > 0) { ?>
            <?php echo $train['number'] . "/" . $train['name']; ?> Train Schedule 🕒  Time table | Train route
        <?php } else { ?>
                Train Schedule 
        <?php } ?>
    </title>
    <meta name="description" content="<?php if (isset($route) && count($route) > 0) { echo $train['number'] . " / " . $train['name']; ?> Train route, schedule/timings details given here. Also check complete route on map with time table.<?php } ?>">

    <?php $this->load->view("include/header") ?>
</head>
<body>
    <?php $this->load->view("include/nav") ?>
	<div class="container custom-container">
        <?php if (isset($route) && count($route) > 0) { ?>
        <ul class="breadcrumb">
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/" itemprop="url"><span itemprop="title">Indiaruler</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/train-schedule" itemprop="url"><span itemprop="title">Train Schedule</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb" class="active"><a href="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" itemprop="url"><span itemprop="title"><?php echo $train['number'] . " " . $train['name'];?></span></a></li>
        </ul>
        <h1><?php echo $train['number'] . " / " . strtoupper($train['name']);?> Train Schedule</h1>
        <p><?php echo $train['name'] . ", " . $train['number']?> is scheduled to travel from <?php echo $route[0]['fullname'];?> to <?php echo $route[count($route) - 1]['fullname'];?> and passes <?php echo count($route) - 2;?> stations 
        <?php
        
            $str = '' ;
            $stationsArray = array();
            foreach ($route as $value) {
                $stationsArray[] = $value['fullname'];
            }
            $first3Array = array_slice($stationsArray, 1, 3);
            $first3 = strtolower(implode(', ', $first3Array));
            $last3Array = array_slice($stationsArray, -4, -1);
            $last3 = strtolower(implode(', ', $last3Array));
            
            echo ", the first ". count($first3Array) . " stations after source are " . $first3 . " and last " . count($last3Array) . " stations before destination are " . $last3;
        
        ?>. The train is due for journey on 
        <?php
            $str = '';
            foreach ($train['days'] as $value) {
                if($value['runs'] === "Y") {
                    $str .= $value['day-code'] . ", ";
                }
            }
            echo rtrim($str, ', ');
        ?>
        . The journey, as per schedule, takes <?php echo $route[count($route) - 1]['day'] ?> days. <?php echo $route[count($route) - 1]['fullname'];?> is the station where the train’s route ends. It covers a total distance of <?php echo haversineGreatCircleDistance($route[0]['lat'], $route[0]['lng'], $route[count($route) - 1]['lat'], $route[count($route) - 1]['lng']) ?> kms.</p>
        <?php } else { ?>
        <ul class="breadcrumb">
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/" itemprop="url"><span itemprop="title">Indiaruler</span></a></li>
            <li itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a href="https://indiaruler.com/train-schedule" itemprop="url"><span itemprop="title">Train Schedule</span></a></li>
        </ul>
        <h1>Train Schedule</h1>
        <p>Train schedule is the feature that list out the expected schedule of a train i.e., its arrival and departure times for various stations etc
This feature helps you to easily get your hands on the information regarding all train schedules.
Simply typing in the train number or name, you can derive the train name, the destinations it covers, the main stations covered by the train, its departing and arrival points etc. Also, you can check particular date-wise schedules.
</p>
        <?php } ?>
		<form class="form-inline" method="post" action="" id="train-form">
			<div class="form-group">
				<input type="text" class="form-control" name="train_number" placeholder="Train Number" id="trainnumber" required>
			</div>
			<div class="form-group"><button class="btn btn-info">Go</button></div>
		</form>
        <?php if (isset($route) && count($route) > 0) { ?>
        <div class="row">
            <br>
            <div class="col-md-8">
                <table class="table table-bordered table-condesed table-striped" id="table">
                    <thead>
                        <th class="text-center">No</th>
                        <th class="text-center">Station</th>
                        <th class="text-center">State</th>
                        <th class="text-center">Days</th>
                        <th class="text-center">Arrival</th>
                        <th class="text-center">Depature</th>
                        <th class="text-center">Distance</th>
                    </thead>
                    <tbody>
                    <?php foreach ($route as $value) { ?>
                        <tr>
                            <td><?php echo $value['no'];?></td>
                            <td>
                                <?php
                                    $linkInfo = getLink('live_station', array("stationCode"=> $value['code'], "stationName" => $value['fullname']));
                                ?>
                                <a href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $value['fullname']; ?></a>
                            </td>
                            <td><?php echo $value['state'];?></td>
                            <td><?php echo $value['day'];?></td>
                            <td><?php echo $value['scharr'];?></td>
                            <td><?php echo $value['schdep'];?></td>
                            <td><?php echo $value['distance'];?></td>
                        </tr>
                    <?php } ?>    
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <p></p>
                <h4>
                    Also Check:
                </h4>
                <p>
                <?php 
                    $train_number = $train['number'];
                    $train_name = $train['name'];       
                    $linkInfo = getLink('running_status', array("train_number"=> $train_number, "train_name" => $train_name));?> 
                    <a class="btn btn-info  btn-interlink" href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>

                    <?php $linkInfo = getLink('seat_avail', array("train_number"=> $train_number, "train_name" => $train_name));?> 
                    <a class="btn btn-info  btn-interlink" href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>

                    <?php $linkInfo = getLink('fare_inquiry', array("train_number"=> $train_number, "train_name" => $train_name));?> 
                    <a class="btn btn-info  btn-interlink" href="<?php echo $linkInfo['href']; ?>" target="_blank" title="<?php echo $linkInfo['title']; ?>"><?php echo $linkInfo['title']; ?></a>
                </p>
            </div>
            <?php $this->load->view('include/map'); ?>
        </div>
        <?php } elseif(isset($route) && count($route) == 0) { ?>
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Could not find any data.
            </div>
        <?php } ?>
	</div>
    <?php $this->load->view('include/footer'); ?>
    <script type="text/javascript">
        <?php if(isset($route)) { ?>
            $(document).ready(function() {
                var map, hasInstalledExtension = false;

                /** Master data array */
                var routeData = <?php echo json_encode($route);?>;

                for (var i = 0; i < routeData.length; i++) {
                    if(routeData[i].lat === 0 || routeData[i].lng === 0) {
                        routeData.splice(i, 1);
                    }
                }

                // fetch source from data
                var source = routeData[0].fullname + ", " + routeData[0].state;

                // fetch destination from data
                var destination = routeData[routeData.length - 1].fullname + ", " + routeData[routeData.length - 1].state;
                
                // prepare waypoints data array
                var waypts = [];
                for (var i = 1; i < routeData.length - 1; i++) {
                    waypts.push({
                      location: routeData[i].fullname + ", " + routeData[i].state,
                      stopover: true
                    });
                }

                // console.log("SOURCE >>> ", source, "DESTINATION >>> ", destination, " WAY POINTS >>> ", waypts);

                function initMap() {

                    // var directionsService = new google.maps.DirectionsService;
                    // var directionsDisplay = new google.maps.DirectionsRenderer;
                    
                    var el = hasInstalledExtension ? document.getElementById('map2') : document.getElementById('map');

                    map = new google.maps.Map(el, {
                      center: {lat: 22.6098, lng: 77.7694},
                      zoom: 6,
                      scrollwheel: false,
                      mapTypeId: google.maps.MapTypeId.ROADMAP
                    });

                    var positions = [],
                    markers = [],
                    infowindow = [],
                    destIndex = routeData.length - 1;

                    var min = [180, 180],
                        max = [-180, -180];
                    
                    var len = routeData.length;

                    var icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==',
                    origin =  "<?php echo base_url(); ?>assets/images/origin.png",
                    destination =  "<?php echo base_url(); ?>assets/images/destination.png",
                    route =  "<?php echo base_url(); ?>assets/images/route.png", 
                    markerImage = null;

                    for (var i = 0; i < len; i++) {
                        
                        var station = routeData[i];
                        
                        if (!station.lat || !station.lng) {
                            continue; 
                        }

                        markerImage = null;

                        if(i === 0) {
                            markerImage = origin;
                        } else if(i === len - 1){
                            markerImage = destination;
                        } else {
                            markerImage = route;
                        }

                        positions.push(new google.maps.LatLng(station.lat, station.lng));
                        
                        min[0] = Math.min(min[0], station.lat);
                        min[1] = Math.min(min[1], station.lng);
                        max[0] = Math.max(max[0], station.lat);
                        max[1] = Math.max(max[1], station.lng);
                        
                        markers.push(new google.maps.Marker({
                            position: positions[i],
                            map: map,
                            icon: markerImage  
                        }));

                        (function  (i) {
                            google.maps.event.addListener(markers[i], 'mouseover', function() {
                                 infowindow[i] = new google.maps.InfoWindow({content: routeData[i].fullname });
                                 infowindow[i].open(map, markers[i]);
                            });

                            google.maps.event.addListener(markers[i], 'mouseout', function() {
                                 infowindow[i].close();
                            });

                        })(i);
                    }

                    map.setCenter(new google.maps.LatLng(((max[0] + min[0]) / 2.0), ((max[1] + min[1]) / 2.0)));
                    map.fitBounds(new google.maps.LatLngBounds(new google.maps.LatLng(min[0], min[1]), new google.maps.LatLng(max[0], max[1])));
                    

                    var trainRoute = new google.maps.Polyline({ map: map, path: positions, strokeColor: '#0070B1', strokeOpacity: 0.6, strokeWeight: 5 });
                    
                    // directionsDisplay.setMap(map);

                    // calculateAndDisplayRoute(directionsService, directionsDisplay);
                }

                function calculateAndDisplayRoute(directionsService, directionsDisplay) {
                    
                    directionsService.route({
                        origin: source,
                        destination: destination,
                        waypoints: waypts,
                        optimizeWaypoints: true,
                        travelMode: "DRIVING"
                    }, function(response, status) {
                        if (status === 'OK') {
                            directionsDisplay.setDirections(response);
                            // var route = response.routes[0];
                            // var summaryPanel = document.getElementById('directions-panel');
                            // summaryPanel.innerHTML = '';
                            // // For each route, display summary information.
                            // for (var i = 0; i < route.legs.length; i++) {
                            //   var routeSegment = i + 1;
                            //   summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
                            //       '</b><br>';
                            //   summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                            //   summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                            //   summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
                            // }
                        } else {
                            if(hasInstalledExtension) {
                                alert("Could not plot route on map.");
                            }
                            console.log('Directions request failed due to ' + status);
                        }
                    });
                }

                function detectExtension(extensionId, callback) { 
                        var img; 
                        img = new Image(); 
                        img.src = "chrome-extension://" + extensionId + "/images/icon.png"; 
                        img.onload = function() { 
                            callback(true); 
                        }; 
                        img.onerror = function() { 
                            callback(false); 
                        }; 
                }

                function showMap() {
                    /** If mobile device then always show map */
                    if(isMobile()) {
                        $("#blurry").remove();
                        hasInstalledExtension = true; // assume ext is installed
                        var intr = setInterval(function () {
                                if(google) {
                                    initMap();
                                    clearInterval(intr);
                                }
                            }, 1000);
                    } else {
                        detectExtension('ffnhfeiahlmjjkonmpfcipljofhgnkli', function(res){
                            if(res) {
                                hasInstalledExtension = true;
                                $("#blurry").remove();
                            } else {
                                $("#nonBlurry").remove();
                            }
                            var intr = setInterval(function () {
                                if(google) {
                                    initMap();
                                    clearInterval(intr);
                                }
                            });
                        });
                    }

                    
                    /** Check if use has extension installed */

                }

                showMap();

                $('#table').dataTable({searching: false, paging: false, responsive: true, "aaSorting": []});
            });

        <?php } ?>
        $(document).ready(function($) {
            $("#train-form").on('submit', function(e){
                e.preventDefault();
                
                var trainnumber = $("#trainnumber").val();

                if(trainnumber.match(/\d+/g)[0].length === trainnumber.length){
                    trainnumber = availableTags.filter(function(trainName){
                        return trainName.indexOf(trainnumber) !== -1;
                    })[0];
                }

                var url = trainnumber.replace(/\D+/g, '') + '-train-route';
                
                window.location.href = window.location.origin + '/train-schedule/' + url;

                // $(this).submit();
            });
        });
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBE_JyCcLx6Y75jvuqDjkveRgSgrlpyIk4">
    </script>
</body>

</html>
