  <div class="navbar navbar-default custom-nav-bg">
      <div class="container-fluid">
        <div class="navbar-header">
          <a href="../" class="navbar-brand">
            <img src="<?php echo base_url(); ?>assets/images/ir.png" alt="India Ruler - Find Indian Railways Information">
          </a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <!-- <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">About Us</a></li>
            <li><a href="#">Contact Us</a></li>
          </ul>
        </div> -->
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li class="#"><a href="<?php echo base_url(); ?>train-running-status">Train Running Status</a></li>
            <li class=""><a href="<?php echo base_url(); ?>cancelled-trains">Cancelled Trains</a></li>
            <li class="#"><a href="<?php echo base_url(); ?>pnr-status">PNR Status</a></li>
            <li class=""><a href="<?php echo base_url(); ?>seat-availability">Seat Availability</a></li>
            <li class=""><a href="<?php echo base_url(); ?>fare-enquiry">Fare Enquiry</a></li>
            <li class=""><a href="<?php echo base_url(); ?>trains-between-stations">Train Between Stations</a></li>
            <li class=""><a href="<?php echo base_url(); ?>train-schedule">Train Schedule</a></li>
            <li class=""><a href="<?php echo base_url(); ?>railway-station">Live Station</a></li>
          </ul>
        </div>
      </div>
    </div>
<!--   <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
        <ul class="nav navbar-nav">
          <li class="#"><a href="<?php echo base_url(); ?>index.php/home/live_train_status">Live Train Status</a></li>
          <li class=""><a href="<?php echo base_url(); ?>index.php/home/cancelled_trains">Cancelled Trains</a></li>
          <li class="#"><a href="<?php echo base_url(); ?>index.php/home/pnr_status">PNR Status</a></li>
          <li class=""><a href="<?php echo base_url(); ?>index.php/home/seat_availability">Seat Availability</a></li>
          <li class=""><a href="<?php echo base_url(); ?>index.php/home/fare_enquiry">Fare Enquiry</a></li>
          <li class=""><a href="<?php echo base_url(); ?>index.php/home/train_station">Train Between Stations</a></li>
          <li class=""><a href="<?php echo base_url(); ?>index.php/home/train_schedule">Train Schedule</a></li>
          <li class=""><a href="<?php echo base_url(); ?>index.php/home/live_station">Live Station</a></li>
        </ul>
      </div>
    </div>
  </nav> -->