<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main';
// $route['live-train-status'] = 'main/live_train_status';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['sitemap.xml'] = "sitemap/index";
$route['api/stations'] = "main/getStations";
$route['pnr-status'] = "main/pnr_status";
$route['cancelled-trains'] = "main/cancelled_trains";


/** Train Running Status Routes */
$route['train-running-status'] = "main/live_train_status";
$route['train-running-status/(:num)'] = "main/live_train_status/$1";


$route['seat-availability'] = "main/seat_availability";
$route['seat-availability/(:num)'] = "main/seat_availability_by_train_number/$1";
$route['seat-availability/(:any)-to-(:any)/(:any)-(:any)'] = "main/seat_availability/$3/$4/$1/$2";

/** Fare inquiry routes */
$route['fare-enquiry'] = "main/fare_enquiry";
$route['fare-enquiry/(:num)'] = "main/fare_enquiry/$1";

/** Train Between Stations Routes */
$route['trains-between-stations'] = "main/train_station";
$route['trains-between-stations/(:any)-to-(:any)/(:any)-(:any)'] = "main/train_station/$3/$4/$1/$2";

/** Train Schedule Routes */
$route['train-schedule'] = "main/train_schedule";
$route['train-schedule/(:num)-train-route'] = "main/train_schedule/$1/$2";


/** Live Station Routes */
$route['railway-station'] = "main/live_station";
$route['railway-station/([a-z]{2,4})-(:any)'] = "main/live_station/$1";