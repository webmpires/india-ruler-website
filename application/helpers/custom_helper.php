<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function pr($array) {
    echo "<pre>";
    print_r($array);
    exit;
}

if ( ! function_exists('getLink'))
{
	function getLink($type, $data) {
        $array = array();
		switch ($type) {
			case 'running_status':
				$url = $data['train_number'];
                $array['href'] = base_url() . 'train-running-status/' . $url;
                $array['title'] = strtoupper($data['train_number'] . "/" . $data['train_name']) . " Running Status";
                $array['text'] = strtoupper($data['train_number'] . " " . $data['train_name']) . " Running Status";
				break;
			case 'fare_inquiry':
				$url = $data['train_number'];
                $array['href'] = base_url() . 'fare-enquiry/' . $url;
                $array['title'] = strtoupper($data['train_number'] . "/" . $data['train_name']) . " Fare";
                $array['text'] = strtoupper($data['train_number'] . " " . $data['train_name']) . " Fare";
				break;

			case 'train_bw_stations':
				# code...
				break;
            
      case 'seat_avail':
				$url = $data['train_number'];
                $array['href'] = base_url() . 'seat-availability/' . $url;
                $array['title'] = strtoupper($data['train_number'] . "/" . $data['train_name']) . " Seat Availability";
                $array['text'] = strtoupper($data['train_number'] . " " . $data['train_name']) . " Seat Availability";
				break;

			case 'schedule':
				$trainInfo = $data['train_number'];
				$pattern = '/\s+$/';
				$trainInfo = strtolower(implode('-', explode(' ', preg_replace($pattern, "", $trainInfo)))) . '-train-route';
                
                $array['href'] = base_url() . 'train-schedule/' . $trainInfo;
                $array['title'] = strtoupper($data['train_number'] . "/" . $data['train_name']) . " Train Schedule";
                $array['text'] = strtoupper($data['train_number'] . " " . $data['train_name']) . " Train Schedule";
				break;

			case 'live_station':
				$url = strtolower($data['stationCode']) . '-' . strtolower(implode('-', explode(' ', $data['stationName'])));
                $array['href'] = base_url() . 'railway-station/' . $url;
                $array['title'] = $data['stationName'] . " Railway Station";
                $array['text'] = $data['stationName'] . " Railway Station";
				break;
			
			default:
				# code...
				break;
		}
		return $array;
	}
}

function haversineGreatCircleDistance(
  $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371)
{
  // convert from degrees to radians
  $latFrom = deg2rad($latitudeFrom);
  $lonFrom = deg2rad($longitudeFrom);
  $latTo = deg2rad($latitudeTo);
  $lonTo = deg2rad($longitudeTo);

  $latDelta = $latTo - $latFrom;
  $lonDelta = $lonTo - $lonFrom;

  $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
    cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
  return number_format($angle * $earthRadius, 2);
}