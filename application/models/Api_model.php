<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model {

	public function __construct() {
            parent::__construct();
            // Your own constructor code
    }

	public function returnMatchingStations($matchText) {
		$this->db->select("station_name, station_code");
		$this->db->like('station_name', $matchText);
		$query = $this->db->get('stations');
 		$array = array();
 		foreach ($query->result_array() as $value) {
 			$array[] = $value['station_name'] . " - " . $value['station_code'];
 		}
 		return $array;
	}

	public function returnMatchingTrains($matchText) {
		$this->db->select("train_desc");
		$this->db->like('train_desc', $matchText);
		$query = $this->db->get('trains');
 		$array = array();
 		foreach ($query->result_array() as $value) {
 			$array[] = $value['train_desc'];
 		}
 		return $array;
	}

	public function validateTrain($train_number, $train_name) {
		$q = $this->db->get_where("trains", array("train_number" => $train_number, 'train_name' => $train_name));
		if(count($q->result()) > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function getTrainNameByNumber($train_number) {
		$q = $this->db->get_where("trains", array("train_number" => $train_number));
		$result = $q->first_row();
		return is_object($result) ? $result->train_name : '';
	}
}

/* End of file Home_model.php */
/* Location: ./application/models/Home_model.php */