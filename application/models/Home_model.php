<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

	public function __construct() {
            parent::__construct();
            // Your own constructor code
    }

    private function sendRequest($url) {
    	// Create new Guzzle client
		$client = new Guzzle\Http\Client("http://api.railwayapi.com", array(
		  'request.options' => array(
		     'exceptions' => false,
		   )
		));

		$url = $url . '/apikey/i8aoaiz1/';
		
		// Prepare request to be sent
		$request = $client->get($url);

		// Send request and capture response
		$response = $request->send();
		// pr($response);
		$statuscode = $response->getStatusCode();

		if($statuscode > 300) {
			$data = array();
			
		} else {
			$data = $response->json();
		}

		return $data;
    }

	public function getPnrStatus() {
		// Set URL
		$url = '/pnr_status/pnr/' . $this->input->post('pnr_number');
		return $this->sendRequest($url);
	}

	public function getCancelledTrains() {
		if($this->input->post('date')) {
			$date = date('d-m-Y', strtotime($this->input->post('date')));
		} else {
			$date = date('d-m-Y');
		}

		$url = '/cancelled/date/' . $date;

		return $this->sendRequest($url);
	}

	public function getSeatAvailibility($sourceCode, $destCode, $date = '') {

		$date = date('d-m', strtotime($date));
		
		$url = '/between/source/'. $sourceCode .'/dest/'. $destCode .'/date/' . $date;

		return $this->sendRequest($url);
	}

	public function checkSeatAvailibility($data) {
		$sourceCode = $data['source'];
		
		$destCode = $data['destination'];
		if(!isset($data['date'])) {
			$data['date'] = date("Y-m-d");
		}
		$date = date('d-m-Y', strtotime($data['date']));
		
		$url = "/check_seat/train/".$data["train_number"]."/source/".$sourceCode."/dest/".$destCode."/date/".$date."/class/".$data["class"]."/quota/".$data["quota"];

		return $this->sendRequest($url);
	}

	public function fareInquiry($params) {
		$sourceCode = $params['sourceCode'];
		$destCode = $params['destCode'];
		$trainNumber = $params['train_number'];		

		$date = date('d-m-Y', strtotime($params['date']));
		
		$url = '/fare/train/'.$trainNumber.'/source/'.$sourceCode.'/dest/'.$destCode.'/age/'.$params['age'].'/quota/'.$params['quota'].'/doj/' . $date;

		return $this->sendRequest($url);
	}


	public function trainBetweenStation($sourceCode, $destCode) {
		$date = date('d-m');
		
		$url = '/between/source/'. $sourceCode .'/dest/'. $destCode .'/date/' . $date;

		return $this->sendRequest($url);
	}

	public function trainSchedule($train_number) {
		$url = '/route/train/' . $train_number;
		return $this->sendRequest($url);
	}

	public function getLiveTrainStatus($sourceCode = 0, $date = '') {
		
		// $tmp = explode(' ', $this->input->get_post('train_number'));

		// $sourceCode = trim($tmp[0]);
		
		// echo "<pre>";
		// print_r($tmp);
		// exit();

		$url = '/live/train/' . $sourceCode .'/doj/' . $date;
		$data = $this->sendRequest($url);
		
		// echo "<pre>";
		// print_r($data);
		// exit();

		$lastRoute = 0;
		if(isset($data) && isset($data['route']) && count($data['route']) > 0) {
			$currentStation = $data['current_station'];
			$existsInRoute = false; // flag indicating if current station exists in route or not
			
			for ($i = 0; $i < count($data['route']); $i++) { 
				
				if($data['route'][$i]['has_arrived'] == 1){
					$lastRoute = $i;
				} 

				if($currentStation['station_']['code'] === $data['route'][$i]['station_']['code']) {
					$existsInRoute = true;
					break;
				}
			}

			
			if($existsInRoute) {
				return $data;
			} else {
				// echo "<pre>";
				// Add current route after last arrived route
				// $currentStation['station_']['name'] = 'New Route';
				array_splice( $data['route'], $lastRoute + 1, 0, array($currentStation));
				// print_r($data['route']);
				// exit;
				return $data;	
			}
		}
	}

	public function liveStation($time, $sourceCode) {
		switch ($time) {
			case '2hours':
				$hours = 2;
				break;
			case '4hours':
				$hours = 4;
				break;
			default:
				$hours = 24;
				break;
		}

		$url = '/arrivals/station/'. $sourceCode .'/hours/'. $hours;
		return $this->sendRequest($url);
	}
}

/* End of file Home_model.php */
/* Location: ./application/models/Home_model.php */