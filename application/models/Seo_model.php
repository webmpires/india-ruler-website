<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seo_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function saveSiteMap($urlsData, $name)
    {
        $xmlString = '<?xml version="1.0" encoding="UTF-8"?>
        <urlset 
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" 
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" 
        xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        $xmlString .= $urlsData;

        $xmlString .= '</urlset>';

        $dom = new DOMDocument;
        $dom->preserveWhiteSpace = TRUE;
        $dom->loadXML($xmlString);

        //Save XML as a file
        $dom->save('assets/sitemap/' . $name);
    }

    /** Website Links Sitemap */
    public function map_websitelinks()
    {
        $links = $this->db->get('website_links');
        $links_data = $links->result();
        if (count($links_data) > 0) {
            $xmlString = '';
            foreach ($links_data as $val) {
                $xmlString .= "<url>
    <loc>" . $val->loc . "</loc>
    <lastmod>" . date('c', strtotime($val->timestamp)) . "</lastmod>
    <changefreq>yearly</changefreq>
    <priority>" . $val->priority . "</priority>
</url>";
            }

            $this->saveSiteMap($xmlString, 'website_links.xml');
        } else {
            $data = file_get_contents("./assets/website_links.json");
            $decoded_data = json_decode($data);
            $xmlString = '';
            foreach ($decoded_data as $val) {
                $urlSlug = implode("-", explode(' ', $val));
                $xmlString .= "<url>
    <loc>" . base_url() . $urlSlug . "</loc>
    <timestamp>" . date('c', time()) . "</timestamp>
    <priority>0.8</priority>
</url>";
            }

            $this->saveSiteMap($xmlString, 'website_links.xml');

            $dataArray = array();
            foreach ($decoded_data as $val) {
                $urlSlug = implode("-", explode(' ', $val));
                $dataArray[] = array(
                    'loc' => base_url() . $urlSlug,
                    'timestamp' => date('Y-m-d H:i:s'),
                    'priority' => 0.8
                );
            }

            $this->saveSiteMapToDB($dataArray, 'website_links');
        }
    }

    /** Train Running Status Sitemap */
    public function map_trainRunningStatus()
    {
        /** Live Train Status Links */
        $trainStatus = $this->db->get('train_running_status');
        $status_data = $trainStatus->result();
        if (count($status_data) > 0) {
            $xmlString = '';
            foreach ($status_data as $val) {
                $xmlString .= "<url>
    <loc>" . $val->loc . "</loc>
    <lastmod>" . date('c', strtotime($val->timestamp)) . "</lastmod>
    <changefreq>hourly</changefreq>
    <priority>" . $val->priority . "</priority>
</url>";
            }

            $this->saveSiteMap($xmlString, 'train_running_status.xml');
        } else {
            $data = file_get_contents("./assets/train_status.json");
            $decoded_data = json_decode($data);

            $xmlString = '';

            foreach ($decoded_data as $val) {
                $tmp = explode(' ', $val);
                $urlSlug = $tmp[0];
                echo $urlSlug . "<br>";
                $xmlString .= "<url>
    <loc>" . base_url() . "train-running-status/" . strtolower($urlSlug) . "</loc>
    <timestamp>" . date('c', time()) . "</timestamp>
    <priority>1.00</priority>
</url>";

            }

            $this->saveSiteMap($xmlString, 'train_running_status.xml');

            $dataArray = array();
            foreach ($decoded_data as $val) {
                $tmp = explode(' ', $val);
                $urlSlug = $tmp[0];
                $dataArray[] = array(
                    'loc' => base_url() . "train-running-status/" . strtolower($urlSlug),
                    'timestamp' => date('Y-m-d H:i:s'),
                    'priority' => 1.00
                );
            }

            $this->saveSiteMapToDB($dataArray, 'train_running_status');

        }
    }

    /** Train Schedule Sitemap */
    public function map_trainSchedule()
    {
        /** Live Train Status Links */
        $trainSchedule = $this->db->get('train_schedule');
        $schedule_data = $trainSchedule->result();
        if (count($schedule_data) > 0) {
            $xmlString = '';
            foreach ($schedule_data as $val) {
                $xmlString .= "<url>
    <loc>" . $val->loc . "</loc>
    <lastmod>" . date('c', strtotime($val->timestamp)) . "</lastmod>
    <changefreq>weekly</changefreq>
    <priority>" . $val->priority . "</priority>
</url>";
            }

            $this->saveSiteMap($xmlString, 'train_schedule.xml');
        } else {
            /** Live Train Status Links */
            $data = file_get_contents("./assets/train_status.json");
            $decoded_data = json_decode($data);

            $xmlString = '';

            foreach ($decoded_data as $val) {
                $tmp = explode(' ', $val);
                $urlSlug = $tmp[0];
                $xmlString .= "<url>
    <loc>" . base_url() . "train-schedule/" . strtolower($urlSlug) . "-train-route</loc>
    <timestamp>" . date('c', time()) . "</timestamp>
    <priority>1.00</priority>
</url>";

            }

            $this->saveSiteMap($xmlString, 'train_schedule.xml');

            $dataArray = array();
            foreach ($decoded_data as $val) {
                $tmp = explode(' ', $val);
                $urlSlug = $tmp[0];
                $dataArray[] = array(
                    'loc' => base_url() . "train-schedule/" . strtolower($urlSlug) . "-train-route",
                    'timestamp' => date('Y-m-d H:i:s'),
                    'priority' => 1.00
                );
            }

            $this->saveSiteMapToDB($dataArray, 'train_schedule');
        }
    }

    /** Train Seat Avail Sitemap */
    public function map_seatAvail()
    {
        $seats = $this->db->get('seat_availability');
        $seats_data = $seats->result();
        if (count($seats_data) > 0) {
            $xmlString = '';
            foreach ($seats_data as $val) {
                $xmlString .= "<url>
    <loc>" . $val->loc . "</loc>
    <lastmod>" . date('c', strtotime($val->timestamp)) . "</lastmod>
    <changefreq>hourly</changefreq>
    <priority>" . $val->priority . "</priority>
</url>";
            }

            $this->saveSiteMap($xmlString, 'seat_availability.xml');
        } else {
            /** Live Train Status Links */
            $data = file_get_contents("./assets/train_status.json");
            $decoded_data = json_decode($data);

            $xmlString = '';

            foreach ($decoded_data as $val) {
                $tmp = explode(' ', $val);
                $urlSlug = $tmp[0];
                $xmlString .= "<url>
    <loc>" . base_url() . "seat-availability/" . strtolower($urlSlug) . "</loc>
    <timestamp>" . date('c', time()) . "</timestamp>
    <priority>1.00</priority>
</url>";

            }

            $this->saveSiteMap($xmlString, 'seat_availability.xml');

            $dataArray = array();
            foreach ($decoded_data as $val) {
                $tmp = explode(' ', $val);
                $urlSlug = $tmp[0];
                $dataArray[] = array(
                    'loc' => base_url() . "seat-availability/" . strtolower($urlSlug),
                    'timestamp' => date('Y-m-d H:i:s'),
                    'priority' => 1.00
                );
            }

            $this->saveSiteMapToDB($dataArray, 'seat_availability');
        }
    }

    /** Train Fair Inquiry Sitemap */
    public function map_fairEnquiry()
    {
        /** Live Train Status Links */
        $enquiry = $this->db->get('fare_enquiry');
        $enquiry_data = $enquiry->result();
        if (count($enquiry_data) > 0) {
            $xmlString = '';
            foreach ($enquiry_data as $val) {
                $xmlString .= "<url>
    <loc>" . $val->loc . "</loc>
    <lastmod>" . date('c', strtotime($val->timestamp)) . "</lastmod>
    <changefreq>daily</changefreq>
    <priority>" . $val->priority . "</priority>
</url>";
            }

            $this->saveSiteMap($xmlString, 'fare_enquiry.xml');
        } else {
            $data = file_get_contents("./assets/train_status.json");
            $decoded_data = json_decode($data);

            $xmlString = '';

            foreach ($decoded_data as $val) {
                $tmp = explode(' ', $val);
                $urlSlug = $tmp[0];
                $xmlString .= "<url>
    <loc>" . base_url() . "fare-enquiry/" . strtolower($urlSlug) . "</loc>
    <timestamp>" . date('c', time()) . "</timestamp>
    <priority>1.00</priority>
</url>";

            }

            $this->saveSiteMap($xmlString, 'fare_enquiry.xml');

            $dataArray = array();
            foreach ($decoded_data as $val) {
                $tmp = explode(' ', $val);
                $urlSlug = $tmp[0];
                $dataArray[] = array(
                    'loc' => base_url() . "fare-enquiry/" . strtolower($urlSlug),
                    'timestamp' => date('Y-m-d H:i:s'),
                    'priority' => 1.00
                );
            }

            $this->saveSiteMapToDB($dataArray, 'fare_enquiry');
        }
    }


    /** Train Running Status Sitemap */
    public function map_trainsBetweenStations() {
        /** Live Train Status Links */
        $stations = $this->db->get('trains_between_stations');
        $stations_data = $stations->result();
        if (count($stations_data) > 0) {
            $xmlString = '';
            foreach ($stations_data as $val) {
                $xmlString .= "<url>
    <loc>" . $val->loc . "</loc>
    <lastmod>" . date('c', strtotime($val->timestamp)) . "</lastmod>
    <changefreq>daily</changefreq>
    <priority>" . $val->priority . "</priority>
</url>";
            }

            $this->saveSiteMap($xmlString, 'trains_between_stations.xml');
        }
    }

    /** Train Running Status Sitemap */
    public function map_liveStation()
    {
        /** Live Train Status Links */
        $stations = $this->db->get('railway_stations');
        $stations_data = $stations->result();
        if (count($stations_data) > 0) {
            $xmlString = '';
            foreach ($stations_data as $val) {
                $xmlString .= "<url>
    <loc>" . $val->loc . "</loc>
    <lastmod>" . date('c', strtotime($val->timestamp)) . "</lastmod>
    <changefreq>hourly</changefreq>
    <priority>" . $val->priority . "</priority>
</url>";
            }

            $this->saveSiteMap($xmlString, 'railway_stations.xml');
        } else {
            $data = file_get_contents("./hacked/stations.json");
            $decoded_data = json_decode($data);

            $xmlString = '';
            echo "<pre>";
            foreach ($decoded_data as $val) {
                $urlSlug = strtolower($val->station_code) . "-" . strtolower(implode("-", explode(' ', $val->station_name)));
                
                $xmlString .= "<url>
    <loc>" . base_url() . "railway-station/" . strtolower($urlSlug) . "</loc>
    <timestamp>" . date('c', time()) . "</timestamp>
    <priority>1.00</priority>
</url>";

            }

            $this->saveSiteMap($xmlString, 'railway_stations.xml');

            $dataArray = array();
            foreach ($decoded_data as $val) {
                $urlSlug = strtolower($val->station_code) . "-" . strtolower(implode("-", explode(' ', $val->station_name)));
                $dataArray[] = array(
                    'loc' => base_url() . "railway-station/" . strtolower($urlSlug),
                    'timestamp' => date('Y-m-d H:i:s'),
                    'priority' => 1.00
                );
            }

            $this->saveSiteMapToDB($dataArray, 'railway_stations');
        }
    }

    public function populateSiteMap()
    {
        $xmlString = '<?xml version="1.0" encoding="UTF-8"?>
    <urlset 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" 
	xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" 
	xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        $trainScheduleXmlString = '<?xml version="1.0" encoding="UTF-8"?>
    <urlset 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" 
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" 
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        /** Website Links */
        $data = file_get_contents("./assets/website_links.json");
        $decoded_data = json_decode($data);

        foreach ($decoded_data as $val) {
            $urlSlug = implode("-", explode(' ', $val));
            $xmlString .= "<url>
    <loc>" . base_url() . $urlSlug . "</loc>
    <timestamp>2017-02-16T16:55:20+00:00</timestamp>
    <priority>0.8</priority>
</url>";
        }

        /** Live Train Status Links */
        $data = file_get_contents("./assets/train_status.json");
        $decoded_data = json_decode($data);

        foreach ($decoded_data as $val) {
            $urlSlug = implode("-", explode(' ', $val));
            $xmlString .= "<url>
    <loc>" . base_url() . strtolower($urlSlug) . "-running-status</loc>
    <timestamp>2017-02-16T16:55:20+00:00</timestamp>
    <priority>1.00</priority>
</url>";

            $trainScheduleXmlString .= "<url>
    <loc>" . base_url() . strtolower($urlSlug) . "-train-schedule</loc>
    <timestamp>2017-02-20T16:55:20+00:00</timestamp>
    <priority>1.00</priority>
</url>";
        }

        $xmlString .= '</urlset>';
        $trainScheduleXmlString .= '</urlset>';

        $dom = new DOMDocument;
        $dom->preserveWhiteSpace = TRUE;
        $dom->loadXML($xmlString);

        //Save XML as a file
        $dom->save('assets/train-status.xml');

        /** Save Schedule sitemap */
        $dom = new DOMDocument;
        $dom->preserveWhiteSpace = TRUE;
        $dom->loadXML($trainScheduleXmlString);

        //Save XML as a file
        $dom->save('assets/train-schedule.xml');

    }

    // Save sitemap into db.
    public function saveSiteMapToDB($urlsData, $name)
    {
        $this->db->insert_batch($name, $urlsData);
    }

    /** Insert link into respective sitemap table if its new */
    public function insertAndRepopulate($type, $url) {
        switch ($type) {
            case 'website':
                $tableName = 'website_links';
                break;

            case 'running_status':
                $tableName = 'train_running_status';
                break;

            case 'schedule':
                $tableName = 'train_schedule';
                break;

            case 'seat_avail':
                $tableName = 'seat_availability';
                break;
            
            case 'fare_enquiry':
                $tableName = 'fare_enquiry';
                break;

            case 'trains_bw_stations':
                $tableName = 'trains_between_stations';
                break;

            case 'live_station':
                $tableName = 'railway_stations';
                break;

            default:
                break;
        }

        $query = $this->db->get_where($tableName, array("loc" => $url));
        $records = $query->result();
        if(count($records) === 0) {
            $this->db->insert($tableName, array(
                "loc" => $url,
                "timestamp" => date('Y-m-d H:i:s'),
                "priority" => 1
            ));

            switch ($type) {
                case 'website':
                    $this->map_websitelinks();
                    break;

                case 'running_status':
                    $this->map_trainRunningStatus();
                    break;

                case 'schedule':
                    $this->map_trainSchedule();
                    break;

                case 'seat_avail':
                    $this->map_seatAvail();
                    break;
                
                case 'fare_enquiry':
                    $this->map_fairEnquiry();
                    break;

                case 'trains_bw_stations':
                    $this->map_trainsBetweenStations();
                    break;

                case 'live_station':
                    $this->map_liveStation();
                    break;

                default:
                    break;
            }
        }
    }
}

/* End of file Home_model.php */
/* Location: ./application/models/Home_model.php */